class Range {
    private int startRange;
    private int finishRange;



    public int getStartRange() {
        return startRange;
    }

    public void setStartRange(int startRange) {
        this.startRange = startRange;
    }

    public int getFinishRange() {
        return finishRange;
    }

    public void setFinishRange(int finishRange) {
        this.finishRange = finishRange;
    }

    public Range(int startRange, int finishRange){
        this.startRange=startRange;
        this.finishRange= finishRange;

        if(startRange>finishRange){
            System.out.print("Incorrect beginning and end of range ");
        }
    }

    public int countLengthRange(Range r){
        int length= r.getFinishRange()-r.getStartRange();
        if (length<0) return 0;
        return length;
    }
    public static boolean isIntersection(Range a, Range b){
        int s1=a.getStartRange();
        int f1=a.getFinishRange();
        int s2=b.getStartRange();
        int f2=b.getFinishRange();
        if((s2>=s1 & s2<=f1) | (f2>=s1 & f2<=f1) | (s1>=s2 & s1<=f2) | (f1>=s2 & f1<=f2)){
            return true;
        }
        else {
           return false;
        }
    }


}
