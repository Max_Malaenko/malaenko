public class Money {

    private long grivn;
    private byte penny;

    public long getGrivn() {
        return grivn;
    }

    public void setGrivn(long grivn) {
        this.grivn = grivn;
    }

    public byte getPenny() {
        return penny;
    }

    public void setPenny(byte penny) {
        this.penny = penny;
    }

    public Money(long grivn, byte penny) {
        this.grivn = grivn;
        this.penny = penny;
    }

    @Override
    public String toString() {
        if (penny < 10) {
            return "money= " + grivn + ".0" + penny;
        } else return "money= " + grivn + "." + penny;
    }

    public static Money add(Money m1, Money m2){
        long grivn= m1.getGrivn()+m2.getGrivn();
        int penny= m1.getPenny()+m2.getPenny();

        if (penny>99){
            grivn++;
            penny-=100;
        }
        return new Money(grivn,(byte) penny);

    }

    public static Money subtruct(Money m1, Money m2){
        long grivn=m1.getGrivn()-m2.getGrivn();
        int penny=m1.getPenny()-m2.getPenny();

        if (grivn>0 & penny<0){
            grivn--;
            penny+=100;

        }
        if(penny<0 & grivn<0) {
            grivn ++;
            penny += 100;
        }
        return new Money(grivn,(byte) penny);
    }

    public static double division(Money m1, Money m2){
        double totalM1=m1.getGrivn()*100+m1.getPenny();
        double totalM2=m2.getGrivn()*100+m2.getPenny();
        return (totalM1/totalM2);
    }

    public static Money divisionByNumber(Money m, double N){
        long totalM= (long)((m.getGrivn()*100+m.getPenny())/N);
        return new Money(totalM/100,(byte)(totalM%100));
    }

    public static Money multiplication(Money m, double N){
        long totalM= (long)((m.getGrivn()*100+m.getPenny())*N);
        return new Money(totalM/100,(byte)(totalM%100));
    }
    public boolean equalsMoney( Money test ){
        return test.getGrivn() == grivn && test.getPenny( ) == penny;
    }

}
