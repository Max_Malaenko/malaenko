/*
Описать базовый класс MainString (Строка). Обязатель-
ные поля класса:
■ массив символов;
■ значение типа int хранит длину строки в символах.
Реализовать обязательные методы следующего назначе-
ния:
■ конструктор без параметров;
■ конструктор, принимающий в качестве параметра стро-
ковый литерал;
■ конструктор, принимающий в качестве параметра символ;
■ метод получения длины строки;
■ метод очистки строки (делает строку пустой);
■ метод поиска символа в строке.
 */
public class MainString {

    private char[] content;
    private int length;

    public char[] getContent() {
        return content;
    }

    public void setContent(char[] content) {
        this.content = content;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public MainString() {

    }

    public MainString(String stroka){
        content=stroka.toCharArray();
        length=content.length;
    }

    public MainString(char ch){
        content=new char[1];
        content[0]=ch;
        length=1;
    }

    public int findLength(){
        length=content.length;
        return length;
    }

    public MainString resetString( MainString obj){
        obj.length=0;
        obj.content=null;
        return obj;
    }

    public void findCharInString(MainString obj, char ch){
        System.out.println(obj.getContent());
        for (int i = 0; i < obj.content.length; i++) {
            if (obj.content[i]==ch){
                System.out.println("index of coincidences = "+i);
            }
        }
    }

}
