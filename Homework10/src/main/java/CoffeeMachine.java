public abstract class CoffeeMachine implements Error{

    protected int volumeForCoffee;
    protected int volumeForWater;
    protected int volumeForUsedCoffee;
    protected boolean error;
    protected boolean isTurnOn;
    protected int amountWater;     // ml
    protected int amountCoffee;    // gram
    protected int amountUsedCoffee; //gram

    public void turnOn() {
        isTurnOn = true;
        System.out.println("Coffee machine is ready");
    }

    public void turnOff() {
        isTurnOn = false;
        System.out.println("Coffee machine is turn off");
    }

    public void cleaningTankUsedCoffee() {
        amountUsedCoffee = 0;
    }

    public abstract void checkWorkingCapacity();

    public void notifyAboutError(){
        System.out.print("Error. ");
    }

    public int getVolumeForCoffee() {
        return volumeForCoffee;
    }

    public void setVolumeForCoffee(int volumeForCoffee) {
        this.volumeForCoffee = volumeForCoffee;
    }

    public int getVolumeForWater() {
        return volumeForWater;
    }

    public void setVolumeForWater(int volumeForWater) {
        this.volumeForWater = volumeForWater;
    }

    public int getVolumeForUsedCoffee() {
        return volumeForUsedCoffee;
    }

    public void setVolumeForUsedCoffee(int volumeForUsedCoffee) {
        this.volumeForUsedCoffee = volumeForUsedCoffee;
    }

    public int getAmountWater() {
        return amountWater;
    }

    public void setAmountWater(int amountWater) {
        this.amountWater = amountWater;
    }

    public int getAmountCoffee() {
        return amountCoffee;
    }

    public void setAmountCoffee(int amountCoffee) {
        this.amountCoffee = amountCoffee;
    }

    public int getAmountUsedCoffee() {
        return amountUsedCoffee;
    }

    public void setAmountUsedCoffee(int amountUsedCoffee) {
        this.amountUsedCoffee = amountUsedCoffee;
    }

}
