public class SecondModel extends FirstModel implements Cappuccino,Latte {
    protected int volumeForMilk;
    protected int amountMilk;


    public SecondModel(int volumeForCoffee, int volumeForWater, int volumeForUsedCoffee, int volumeForMilk) {
        this.volumeForCoffee = volumeForCoffee;
        this.volumeForWater = volumeForWater;
        this.volumeForUsedCoffee = volumeForUsedCoffee;
        this.volumeForMilk = volumeForMilk;

    }
    public void makeCappuccino(int milk) {
        checkWorkingCapacity();
        if (error) return;
        else {
            amountCoffee -= 22;
            amountWater -= 30;
            amountMilk -= milk;
            amountUsedCoffee += 22;
            System.out.println("A cup of cappuccino was made");
        }
    }

    public void makeLatte(int milk) {
        checkWorkingCapacity();
        if (error) return;
        else {
            amountCoffee -= 22;
            amountWater -= 30;
            amountMilk -= milk;
            amountUsedCoffee += 22;
            System.out.println("A cup of latte was made");
        }
    }



    @Override
    public void checkWorkingCapacity() {
        SecondModel cm = this;
        error = false;
        if (!isTurnOn) {
            error = true;
            return;
        }

        if (amountWater <= 0) {
            cm.notifyAboutError();
            System.out.println("No Water");
            error = true;
            return;
        }

        if (amountCoffee <= 0) {
            cm.notifyAboutError();
            System.out.println("No Coffee");
            error = true;
            return;
        }

        if (amountUsedCoffee >= volumeForUsedCoffee) {
            cm.notifyAboutError();
            System.out.println("Tank for used coffee is full");
            error = true;
            return;
        }
        if (amountMilk <= 0) {
            cm.notifyAboutError();
            System.out.println("No Milk");
            error = true;
            return;
        }
    }

    public int getVolumeForMilk() {
        return volumeForMilk;
    }

    public void setVolumeForMilk(int volumeForMilk) {
        this.volumeForMilk = volumeForMilk;
    }

    public int getAmountMilk() {
        return amountMilk;
    }

    public void setAmountMilk(int amountMilk) {
        if(amountMilk>volumeForMilk){
            System.out.println("Too much milk");
        }
        else {
            this.amountMilk = amountMilk;
        }
    }
}




