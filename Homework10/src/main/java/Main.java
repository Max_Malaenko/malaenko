public class Main {
    public static void main(String[] args) {
        /*CoffeeMachine f1= new FirstModel(200,500,300);
        f1.setAmountCoffee(100);
        f1.setAmountWater(250);

        f1.turnOn();
        ((FirstModel)f1).makeAmericano();
        ((FirstModel)f1).makeEspresso();
        ((FirstModel)f1).makeAmericano();
        ((FirstModel)f1).makeAmericano();
        ((FirstModel)f1).makeEspresso();
        f1.cleaningTankUsedCoffee();
        f1.turnOff();

        CoffeeMachine cm2=new SecondModel(250,500,350,400);
        cm2.setAmountWater(195);
        cm2.setAmountCoffee(45);
        ((SecondModel)cm2).setAmountMilk(400);

        ((SecondModel)cm2).turnOn();
        ((SecondModel)cm2).makeLatte(70);
        ((SecondModel)cm2).makeAmericano();
        ((SecondModel)cm2).makeCappuccino(100);
        ((SecondModel)cm2).cleaningTankUsedCoffee();
        ((SecondModel)cm2).makeAmericano();
        ((SecondModel)cm2).makeAmericano();
        ((SecondModel)cm2).turnOff();*/

        CoffeeMachine cm3=new ThirdModel(300,350,200,250,300);
        cm3.setAmountCoffee(200);
        cm3.setAmountWater(200);
        ((ThirdModel)cm3).setAmountMilk(150);
        ((ThirdModel)cm3).setAmountGroundCoffee(150);

        ((ThirdModel)cm3).turnOn();
        ((ThirdModel)cm3).makeAmericano();
        ((ThirdModel)cm3).makeLatte(100);
        ((ThirdModel)cm3).cleaningTankUsedCoffee();
        ((ThirdModel)cm3).makeAmericano();
        ((ThirdModel)cm3).makeAmericano();
        ((ThirdModel)cm3).turnOff();


    }
}
