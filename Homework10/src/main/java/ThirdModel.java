public class ThirdModel extends CoffeeMachine implements Americano,Latte {
    private int volumeForGroundCoffee;
    private int amountGroundCoffee;
    protected int volumeForMilk;
    protected int amountMilk;

    public ThirdModel(int volumeForCoffee, int volumeForWater, int volumeForUsedCoffee, int volumeForMilk, int volumeForGroundCoffee) {
        this.volumeForCoffee = volumeForCoffee;
        this.volumeForWater = volumeForWater;
        this.volumeForUsedCoffee = volumeForUsedCoffee;
        this.volumeForMilk = volumeForMilk;
        this.volumeForGroundCoffee=volumeForGroundCoffee;

    }

    public void makeAmericano(){
        checkWorkingCapacity();
        if(error) return;

        if(amountCoffee<=0 & amountGroundCoffee>0) {
            amountGroundCoffee -= 22;
        }
        else{
            amountCoffee-=22;
        }
        amountWater-=100;
        amountUsedCoffee=+22;
        System.out.println("A cup of americano was made");
    }

    public void makeLatte(int milk){
        if(error) return;
        checkWorkingCapacity();
        if(amountCoffee<=0 & amountGroundCoffee>0) {
            amountGroundCoffee -= 22;
        }
        else{
            amountCoffee-=22;
        }
        amountWater-=30;
        amountMilk-=milk;
        amountUsedCoffee+=22;
        System.out.println("A cup of latte was made");
    }

    @Override
    public void checkWorkingCapacity() {
        ThirdModel cm = this;
        error = false;
        if (!isTurnOn) {
            error = true;
            return;
        }

        if (amountWater <= 0) {
            cm.notifyAboutError();
            System.out.println("No Water");
            error = true;
            return;
        }

        if (amountCoffee <= 0 & amountGroundCoffee<=0) {
            cm.notifyAboutError();
            System.out.println("No Coffee");
            error = true;
            return;
        }

        if (amountUsedCoffee >= volumeForUsedCoffee) {
            cm.notifyAboutError();
            System.out.println("Tank for used coffee is full");
            error = true;
            return;
        }
        if (amountMilk <= 0) {
            cm.notifyAboutError();
            System.out.println("No Milk");
            error = true;
            return;
        }
        if(amountGroundCoffee<=0){
            cm.notifyAboutError();
            System.out.println("No Ground Coffee");
            error=true;
            return;
        }
    }

    public int getVolumeForGroundCoffee() {
        return volumeForGroundCoffee;
    }

    public void setVolumeForGroundCoffee(int volumeForGroundCoffee) {
        this.volumeForGroundCoffee = volumeForGroundCoffee;
    }

    public int getAmountGroundCoffee() {
        return amountGroundCoffee;
    }

    public void setAmountGroundCoffee(int amountGroundCoffee) {
        this.amountGroundCoffee = amountGroundCoffee;
    }

    public int getVolumeForMilk() {
        return volumeForMilk;
    }

    public void setVolumeForMilk(int volumeForMilk) {
        this.volumeForMilk = volumeForMilk;
    }

    public int getAmountMilk() {
        return amountMilk;
    }

    public void setAmountMilk(int amountMilk) {
        this.amountMilk = amountMilk;
    }
}
