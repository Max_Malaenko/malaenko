public class FirstModel extends CoffeeMachine implements Americano,Espresso {

   public FirstModel(){

    }

    public FirstModel(int volumeForCoffee, int volumeForWater, int volumeForUsedCoffee) {
        this.volumeForCoffee = volumeForCoffee;
        this.volumeForWater = volumeForWater;
        this.volumeForUsedCoffee = volumeForUsedCoffee;

    }

    public void makeAmericano() {
       checkWorkingCapacity();
       if (error) return;
        else {
            amountCoffee -= 22;
            amountWater -= 100;
            amountUsedCoffee += 22;
            System.out.println("A cup of americano was made");
        }
    }
    public void makeEspresso() {
        checkWorkingCapacity();
        if (error) return;
        else {
            amountCoffee -= 22;
            amountWater -= 30;
            amountUsedCoffee += 22;
            System.out.println("A cup of espresso was made");
        }
    }

    @Override
    public  void checkWorkingCapacity(){
        FirstModel cm = this;
        error = false;
        if (!isTurnOn) {
            error = true;
            return;
        }

        if (amountWater <= 0) {
            cm.notifyAboutError();
            System.out.println("No water");
            error = true;
            return;
        }
        if (amountCoffee <= 0) {
            cm.notifyAboutError();
            System.out.println("No coffee");
            error = true;
            return;
        }
        if (amountUsedCoffee >= volumeForUsedCoffee) {
            cm.notifyAboutError();
            System.out.println("Tank for used coffee is full");
            error = true;
            return;
        }

    }
}
