import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Напишите программу, которая посчитает количество
смайликов в заданном тексте.
*/
public class Task12 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the string");
        String string= scan.nextLine();
        scan.close();
        int numberOfSmile=countSmile(string);
        System.out.println(numberOfSmile);
    }

    public static int countSmile(String stroka){
        Pattern pattern;
        pattern = Pattern.compile("[:|;]{1}[-]*(\\)*?|\\(*?|\\[*?|\\]*?)"); // не смог дожать regex, чтобы считало в конце только одинаковые скобки
        Matcher matcher=pattern.matcher(stroka);
        int sum=0;
        while(matcher.find()){
            sum++;
        }
        return sum;
    }
}
