import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Слова в языке Мумба-Юмба могут состоять только из букв
a, b, c, и при этом:
- никогда не содержат двух букв b подряд;
- ни в одном слове никогда не встречается три одинаковых
подслова подряд. Например, по этому правилу, в язык
Мумба-Юмба не могут входить слова aaa (так как три
раза подряд содержит подслово a), ababab (так как три
раза подряд содержит подслово ab), aabcabcabca (три
раза подряд содержит подслово abc).
Все слова, удовлетворяющие вышеописанным правилам,
входят в язык Мумба-Юмба.
Напишите программу, которая по данному слову (введе-
ного с клавиатуры) определит, принадлежит ли оно этому
языку.
 */
public class Task11 {

    public static void main(String[] args) {

        Scanner scan=new Scanner(System.in);
        System.out.println("Enter the word");
        String word=scan.nextLine();
        scan.close();

        boolean isLanguage =checkTheWord(word);
        if (isLanguage){
            System.out.println("YES");
        }
        else{
            System.out.println("NO");
        }

    }

    public static boolean checkTheWord(String stroka){
        if (stroka.contains("bb")){
            return false;
        }
        else {
            Pattern pattern=Pattern.compile("([abc]*)(\\1)(\\1)");
            Matcher matcher= pattern.matcher(stroka);
            if (matcher.find()){
                return true;
            }
            else return false;
        }
    }
}
