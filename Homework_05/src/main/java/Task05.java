import java.util.Scanner;
import java.util.regex.Pattern;

/*
Ввести строку с клавиатуры. В строке должны содержаться
слова, которые могут быть раздельные пробелами или
двоеточиями. Необходимо вычислить количество слов в
строке, у которых четное количество букв.
 */
public class Task05 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the string");
        String stroka =scan.nextLine();
        scan.close();

        String[] arrString= splitToSentenceIntoWords(stroka);
        System.out.println(countEvenLettersWords(arrString));
    }

    public static String[] splitToSentenceIntoWords(String s){

    Pattern pattern= Pattern.compile("\\s*\\W+");
    String[] ArrString=pattern.split(s);
    return ArrString;
    }

    public static int countEvenLettersWords(String[] arrStr){
        int sum=0;
        for (int i=0; i<arrStr.length; i++){
            if( arrStr[i].length()%2==0){
                sum++;
            }
        }
        return sum;
    }
}
