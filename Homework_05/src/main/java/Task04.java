import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Ввести строку с клавиатуры (латиницей). Из введенной
строки выбрать все слова, начинающиеся на гласные буквы
и заканчивающиеся на согласные. Вывести отобранные
слова на консоль.
 */
public class Task04 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the string");
        String stroka = scan.nextLine();
        scan.close();

        findWords(stroka);

    }

public static void findWords(String string){

        String[] stringArr=string.split("\\s+");

        Pattern pattern= Pattern.compile("^[aeyuio[AEYUIO]][a-z]+[^aeyuio]$");
        Matcher matcher;

        for(String s : stringArr ){
            matcher=pattern.matcher(s);
            if(matcher.matches()) {
               System.out.println(s);
            }
        }
    }
}
