/*
Пользователь вводит с клавиатуры любую строку.
Поменять в исходной строке все большие буквы на
маленькие, а маленькие – на большие. Если в строке при-
сутствуют цифры, заменить на символ подчеркивания и
вывести результат на консоль.
 */

import java.util.Scanner;

public class Task07 {

    public static void main(String[] args) {

        Scanner scan=new Scanner(System.in);
        System.out.println("Enter the string:");
        String string= scan.nextLine();
        scan.close();

        System.out.println(changeTheString(string));
    }

    public static String changeTheString(String stroka){
        char[] charsArr= stroka.toCharArray();
        for(int i=0; i<charsArr.length; i++){

            if (Character.isUpperCase(charsArr[i])){
                charsArr[i]=Character.toLowerCase(charsArr[i]);
            }
            else if(Character.isLowerCase((charsArr[i]))){
                charsArr[i]=Character.toUpperCase(charsArr[i]);
            }
            else if(Character.isDigit(charsArr[i])){
                charsArr[i]='_';
            }
        }
       StringBuilder sb= new StringBuilder();
       for(int i=0; i<charsArr.length; i++){
           sb.append(charsArr[i]);
       }
       String newString=sb.toString();

       return newString ;

    }

}
