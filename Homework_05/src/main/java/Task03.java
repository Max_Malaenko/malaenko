import java.util.Scanner;

/*
Подсчитать среднюю длину слова, во введенном с
клавиатуры предложении.
 */
public class Task03 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        String stroka = scan.nextLine();
        scan.close();

        String[] arrStroka= stroka.split(" ");

        int[] length=new int[arrStroka.length];
        int sum=0;
        for (int i=0; i<arrStroka.length; i++){
            length[i]=arrStroka[i].length();
            sum+=length[i];
        }

        double averageLength=sum/length.length;
        System.out.println(averageLength);
    }

}

