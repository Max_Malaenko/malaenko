import java.util.Scanner;

/*
Ввести с клавиатуры строку текста, а затем один символ.
Показать на консоль индексы и количество совпадений (ищем
вхождения символа в строку). В случае если совпадений не
найдено, вывести соответствующий текст.
 */
public class Task01 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the string:");
        String stroka =scan.nextLine();
        System.out.println("Enter the symbol");
        char symbol = scan.next().charAt(0);
        scan.close();

        int index=0;
        int sum=0;
        int i=index;
        System.out.print("№: ");
        while(index!=-1){
            index=stroka.indexOf(symbol,i);
            if(index>=0) {
                System.out.print(index+" ");
                sum++;
            }
            i=index+1;
        }
        System.out.println();
        System.out.println("Number of entries= "+sum);

    }
}
