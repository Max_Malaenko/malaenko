import java.util.Scanner;

/*
Напишите программу, которая будет печатать
ромбовидный рисунок на основе строки, введенной с
клавиатуры (максимальная длина – 50 символов).
 */
public class Task10 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the string");
        String inputString= scan.nextLine();
        scan.close();
        drawRhonbus(inputString);

    }

    public static void drawRhonbus(String stroka){
        char[] stringAsChar= stroka.toCharArray();
        int N= stroka.length();

        for (int i = 0; i < N; i++) {           //верхняя часть ромба
            for (int j = 0; j < N-1-i; j++) {
                System.out.print(" ");
            }
                for (int k = 0; k <=i ; k++) {
                System.out.print(stringAsChar[k]);
                }
            System.out.println();
        }

        for (int m = 0; m < N - 1; m++) {       //нижняя часть ромба
            for (int p = m+1; p <N ; p++) {
                System.out.print(stringAsChar[p]);
            }
            System.out.println();
        }

    }
}
