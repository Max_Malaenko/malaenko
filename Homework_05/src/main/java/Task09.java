import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

/*
Написать программу, проверяющую является ли одна
строка анаграммой для другой строки (строка может состоять
из нескольких слов и символов пунктуации). Пробелы и
пунктуация должны игнорироваться при анализе. Разница в
больших и маленьких буквах должна игнорироваться. Обе
строки должны вводиться с клавиатуры. Программа должна
выводить Yes, если строки являются анаграммой, и No –
иначе.
 */
public class Task09 {

    public static void main(String[] args) {

        Scanner scan =new Scanner(System.in);
        System.out.println("Enter the first string");
        String firstString= scan.nextLine();
        System.out.println("Enter the second string");
        String secondString= scan.nextLine();
        scan.close();

        boolean isAnagram=isAnagram(firstString,secondString);
        if (isAnagram){
            System.out.println("YES");
        }
        else {
            System.out.println("NO");
        }

    }
    public static boolean isAnagram(String s1, String s2){


            String firstString = s1.toLowerCase();
            String secondSring = s2.toLowerCase();
            firstString = firstString.replaceAll("\\W+\\s*", "");
            secondSring = secondSring.replaceAll("\\W+\\s*", "");
            char[] charArrFirst = firstString.toCharArray();
            char[] charArrSecond = secondSring.toCharArray();
            Arrays.sort(charArrFirst);
            Arrays.sort(charArrSecond);
            String tempS1= new String(charArrFirst);
            String tempS2= new String(charArrSecond);

            return tempS1.equals(tempS2);

    }
}
