import java.util.Scanner;

/*
В языке Java принято первое слово, входящее в название
переменной, записывать с маленькой латинской буквы.
Следующее слово идет с большой буквы (только первая
буква слова – большая). Слова не имеют разделителей и
состоят только из латинских букв. Например, правильные
записи переменных в Java могут выглядеть следующим
образом: javaIdentifier, longAndMnemonicIdentifier,
name, nEERC.
В языке C++ для описания переменных используются
только маленькие латинские символы и символ «_»,
который отделяет непустые слова друг от друга. Если
строка имеет смешанный синтаксис: например java_
Identifier, сообщить об этом. Примеры: java_identifier,
long_and_mnemonic_identifier, name, n_e_e_r_c.
Вам требуется написать программу, которая преобразует
переменную, записанную на одном языке, в формат другого
языка. Идентификатор (имя) переменной должен вводится с
клавиатуры. Программа должна определить, из какого языка
взята переменная, и переделать ее в переменную другого
языка. Вывести результат на консоль.
 */
public class Task08 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the modifier:");
        String modifier = scan.nextLine();
        scan.close();

        String language = identificationLanguage(modifier);
        System.out.println((language));
        System.out.println(changeLanguage(modifier, language));
    }

    public static String identificationLanguage(String stroka) {
        boolean isUpper = false;
        boolean isUnderscore = false;
        String language = "";
        if (stroka.contains("_")) {
            isUnderscore = true;
        }
        char[] charArr = stroka.toCharArray();

        for (int i = 0; i < charArr.length; i++) {
            if (Character.isUpperCase(charArr[i])) {
                isUpper = true;
                break;
            }
        }

        if (isUpper && isUnderscore) {

            language = "Mixed";
        } else if (isUpper == false && isUnderscore) {

            language = "C++";
        } else if (isUnderscore == false) {

            language = "Java";
        }
        return language;
    }

    public static String changeLanguage(String stroka, String language) {

        String newString = null;
        if (language.equals("Java")) {
            char[] charArr = stroka.toCharArray();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < charArr.length; i++) {
                if (Character.isUpperCase(charArr[i])) {
                    sb.append("_" + Character.toLowerCase(charArr[i]));
                } else sb.append(charArr[i]);
            }
            newString = sb.toString();
            return newString;
        }
        else if(language.equals("C++")) {
            char[] charArr =stroka.toCharArray();
            StringBuilder sb= new StringBuilder();
            for (int i = 0; i < charArr.length; i++) {
                if ((charArr[i]=='_')){
                    charArr[i+1]=Character.toUpperCase(charArr[i+1]);
                }
                sb.append(charArr[i]);
            }
            newString=sb.toString();
            newString=newString.replaceAll("_", "");
            return newString;
        }
        return newString;
    }
}
