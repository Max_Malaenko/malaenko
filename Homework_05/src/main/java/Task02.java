import java.util.Scanner;

/*
Написать программу, которая создаст строку, в которой
находятся все целые числа, начиная с 1, выписаны в одну
строку «123456789101112131415...». Строка должна быть
длиной не более 1 000 символов.
По числу n (введенного с клавиатуры), выведите цифру на
n-й позиции (используется нумерация с 1).
 */
public class Task02 {

    public static void main(String[] args) {

        StringBuilder sb= new StringBuilder();
        int length= sb.length();
        int i=1;

        while (length<1000){
           sb.append(i);
           length++;
           i++;
        }
        String stroka= sb.toString();
        System.out.println(stroka);
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number");
        int number = scan.nextInt();

        System.out.println("Char from number: " +sb.charAt(number-1));
    }
}
