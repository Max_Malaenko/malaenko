package ua.step.homework;
/*
 * Шаблон для решения домашнеего задания. 
 */
/**
 * Проверить, имеет ли число в переменной типа float вещественную часть.
 * Например, числа 3.14 и 2.5 – имеют вещественную часть, а числа 5.0 и 10.0 – нет.
 */
public class Task09
{
    public static void main(String[] args)
    {
        final float testFloat = 3.75f;
        if (testFloat - Math.floor(testFloat)==0){
            System.out.println("Данное число не имеет вещественное части");

        }
        else {
            double ost= testFloat - Math.floor(testFloat);
            System.out.println("Данное число имеет вещественную часть, равную "+ ost);
        }

    }
}
