package ua.step.homework;

/*
 * Шаблон для решения домашнеего задания. 
 */

/**
 * В переменной n хранится натуральное (целое) трехзначное число. Создайте
 * программу, вычисляющую и выводящую на экран сумму цифр числа n при любых
 * значения n.
 */
public class Task02
{
    public static void main(String[] args)
    {
        int n = 375565;
        // строка кода ниже нужна для тестирования (смотри Task2Test) не изменяйте и не удаляйте ее
        n = (args.length == 1) ? Integer.valueOf(args[0]) : n;
        // используй переменную с именем n в качестве входных данных
        int sum= 0;

        while (n>0) {
            sum =sum + (n%10);
            n /= 10;
        }
        System.out.println("Сумма всех цифр числа n = "+ sum);
    }
}