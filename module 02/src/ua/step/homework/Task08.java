package ua.step.homework;
/*
 * Шаблон для решения домашнеего задания. 
 */

import java.util.Scanner;

/**
 * Зная скорость распространения звука в воздушной среде, можно вычислить
 * расстояние до места удара молнии по времени между вспышкой и раскатом грома.
 * Зная время в секундах между вспышкой и раскатом грома (константа в программе)
 * вычислите расстояния до места удара молнии и выведите его на экран.
 */
public class Task08 {
    public static void main(String[] args) {

        final float SPEED = 340.29f; // м/с

        Scanner s =new Scanner(System.in);
        System.out.println("Введите время между вспышкой и раскатом грома");
        final float TIME =s.nextFloat();

        float distance = SPEED*TIME;
        System.out.println("Расстояние = "+ distance+" метров");


    }
}
