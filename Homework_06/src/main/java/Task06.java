import java.util.Scanner;

/*
Нарисовать на экране лесенку. Количество ступенек ука-
зывает пользователь с клавиатуры.
 */
public class Task06 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the number of step");
        int numberOfStep=scan.nextInt();
        scan.close();
        drawSteps(numberOfStep);
    }

    public static void drawSteps(int N){
        for (int i = 0; i < N-1; i++) {
            for (int j = 0; j <2*i; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= 3; k++) {
                System.out.print("*");
            }
            System.out.println();
            for (int p = 0; p < 2*(i+1); p++) {
                System.out.print(" ");
            }
            System.out.print("*");

            System.out.println();
        }
        for (int j = 0; j <2*(N-1) ; j++) {
            System.out.print(" ");

        }
        for (int k = 0; k <3 ; k++) {
            System.out.print("*");

        }

    }
}
