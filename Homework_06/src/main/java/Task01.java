import java.util.Scanner;

/*
Прямоугольный треугольник,
прямым углом вниз-вправо:
заполненный и пустой.
 */
public class Task01 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the height");
        int height = scan.nextInt();
        scan.close();

        drawTriangleFull(height);
        System.out.println();
        drawTriangleEmpty(height);
    }

    public static void drawTriangleFull(int N) {

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N - i - 1; j++) {
                System.out.print(" ");
                }
                for (int k = 0; k <= i; k++) {
                    System.out.print("*");
                }
                 System.out.println();
        }
    }

    public static void drawTriangleEmpty(int N){

        for (int i = 0; i < N; i++) {
            if (i == N - 1) {
                for (int j = 0; j < N; j++) {
                    System.out.print("*");
                }
            } else {
                for (int j = 0; j < N - i - 1; j++) {
                    System.out.print(" ");
                }
                for (int k = 0; k <= i; k++) {
                   if(k==0|k==i) {
                       System.out.print("*");
                   }
                   else {
                       System.out.print(" ");
                   }
                }
            }
            System.out.println();
        }
    }
}

