import java.util.Scanner;

/*
Параллелограмм: заполненный и пустой.
 */
public class Task05 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the height");
        int height = scan.nextInt();
        scan.close();

        drawParallelogramFull(height);
        System.out.println();
        drawParallelogramEmpty(height);
    }

    public static void drawParallelogramFull(int N) {

        int L = 3 * N;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N - i - 1; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < L; k++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void drawParallelogramEmpty(int N) {

        int L = 3 * N;
        for (int i = 0; i < N; i++) {

            if (i == 0 | i == N - 1) {
                for (int m = 0; m < N - 1 - i; m++) {
                    System.out.print(" ");
                }
                for (int p = 0; p < L; p++) {
                    System.out.print("*");
                }
            } else {
                for (int j = 0; j < N - i - 1; j++) {
                    System.out.print(" ");
                }
                for (int k = 0; k < L; k++) {

                    if (k == 0 | k == L - 1) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }
}
