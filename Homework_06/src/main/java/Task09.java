import java.util.Scanner;

/*
Необходимо нарисовать ёлочку символом «звёздочка».
Каждый новый ярус должен быть шире предыдущего. С
клавиатуры вводится количество ярусов, и высота первого
(верхнего) яруса ёлочки (количество строк в ярусе). Ёлочка
должна быть симметричная.
 */
public class Task09 {

    public static void main(String[] args) {

        Scanner scan =new Scanner(System.in);
        System.out.println("Enter the number of the tiers");
        int numberOfTiers = scan.nextInt();
        System.out.println("Enter the height of the first tier");
        int height= scan.nextInt();
        scan.close();

        drawTree(numberOfTiers, height);

    }

    public static void drawTree(int tiers, int height){

        int hLast=height+tiers-1;  // высота последнего яруса

        for (int t = 0; t < tiers; t++) {

            for (int i = 0; i <height+t; i++) {

                for (int j = 0; j <hLast-i-1 ; j++) {   // формируем отступы
                    System.out.print(" ");
                }
                for (int k = 0; k <2*i+1 ; k++) {
                    System.out.print("*");
                }
                System.out.println();
            }
            System.out.println();
        }
    }
}
