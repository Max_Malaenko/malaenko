import java.util.Scanner;

/*
С клавиатуры вводится целое положительное число лю-
бой разрядности. Необходимо перевернуть это число, т. е.
цифры должны располагаться в обратном порядке (напри-
мер, вводим число 1234 – в результате будет 4321).
Не использовать строки и массивы.
 */
public class Task07 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the number");
        int number=scan.nextInt();
        scan.close();

        int reversedNumber=0;
        while(number!=0){
            reversedNumber=reversedNumber*10+number%10;
            number/=10;
        }
        System.out.println(reversedNumber);
    }
}
