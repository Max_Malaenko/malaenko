import java.util.Scanner;

/*
С клавиатуры вводится целое число любой разрядности.
Программа должна определить и вывести на консоль коли-
чество цифр в этом числе, а так же сумму этих чисел.
 */
public class Task08 {

    public static void main(String[] args) {

        Scanner scan =new Scanner(System.in);
        System.out.println("Enter the number");
        int number= scan.nextInt();
        scan.close();

        int count=0;
        int sum =0;
        while(number!=0){
            sum+=number%10;
            number/=10;
            count++;
        }
        System.out.printf("Number of digit = %d \n", count);
        System.out.printf("Sum of digit = %d", sum);
    }
}
