import java.util.Scanner;

/*
Ромб: заполненный и пустой.
 */
public class Task04 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the height");
        int height = scan.nextInt();
        scan.close();

        if(height%2==0){
         height-=1;
        }
        drawRhombusFull(height);


    }

    public static void drawRhombusFull(int N){

        for (int i = 0; i <=(N-1)/2 ; i++) {
            for (int j = 0; j < N - 1 - i; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < 2 * i + 1; k++) {
                System.out.print("*");
            }
            System.out.println();
        }
        for (int i = 0; i <(N-1)/2; i++) {
            for (int j = 1; j <=(N-1)/2 +i+1; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k <N-2*(i+1); k++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
