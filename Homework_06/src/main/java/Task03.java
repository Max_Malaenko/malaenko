import java.util.Scanner;
/*
Равнобедренный треугольник:
заполненный и пустой.
 */
public class Task03 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the height");
        int height=scan.nextInt();
        scan.close();

        drawIsoscelesTriangleFull(height);
        System.out.println();
        drawIsoscelesTriangleEmpty(height);
    }

    public static void drawIsoscelesTriangleFull(int n){

        for (int i=0; i<n; i++){
            for(int k=0; k<n-1-i;k++){
                System.out.print(" ");
            }
            for(int j=0; j<2*i+1;j++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
    public static void drawIsoscelesTriangleEmpty(int n){

        for (int i=0; i<n; i++){
            for(int k=0; k<n-1-i;k++){
                System.out.print(" ");
            }
            for(int j=0; j<2*i+1;j++){
                if(i==n-1){
                    System.out.print("*");
                }
                else {
                    if (j == 0 | j == 2 * i) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }
}
