import java.util.Scanner;

/*
Прямоугольный треугольник,
прямым углом вверх-вправо:
заполненный и пустой.
 */
public class Task02 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the height");
        int height = scan.nextInt();
        scan.close();

        drawRectangularTriangleFull(height);
        System.out.println();
        drawRectangularTriangleEmpty(height);
    }

    public static void drawRectangularTriangleFull(int N){

        for (int i = 0; i < N; i++) {
                for (int j = 0; j < i; j++) {
                    System.out.print(" ");
                }
                for (int k = 0; k < N - i; k++) {
                    System.out.print("*");
                }
               System.out.println();
        }
    }

    public static void drawRectangularTriangleEmpty(int N){
        for (int i = 0; i < N; i++) {
           if (i==0){
                for (int j = 0; j < N; j++) {
                    System.out.print("*");
                }
            }
            else {
               for (int j = 0; j < i; j++) {
                   System.out.print(" ");
               }
               for (int k = 0; k < N - i; k++) {
                   if(k==0|k==N-1-i) {
                       System.out.print("*");
                   }
                   else {
                       System.out.print(" ");
                   }
               }
           }
            System.out.println();
        }
    }
}

