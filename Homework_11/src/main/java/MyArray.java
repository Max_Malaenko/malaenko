

import java.util.Arrays;
import java.util.Random;

public class MyArray<E> implements Cloneable{

    private Object[] data;
    private int size;
    private int capacity;

    /**
     * Default constructor that creates an array for 10 elements
     */
    public MyArray() {
        data = new Object[10];
        capacity = 10;
    }

    /**
     * Constructor that creates an array for given capacity
     * @param initialCapacity
     */
    public MyArray(int initialCapacity) {
        if (initialCapacity >= 0) {
            data = new Object[initialCapacity];
            capacity = initialCapacity;
        } else System.out.println("Illegal capacity");
    }

    /**
     * Adding an element to the end of the array.
     * Must be checking for enough memory! If there is no memory
     * enough to increase the capacity of the data array
     * @param obj
     * @param <E>
     */
    public <E> void pushBack(E obj) {

        ensureCapacity(1);
        data[size] = obj;
        size++;
    }

    /**
     * Removing the first element from an array
     */
    public void popFront() {
        data = Arrays.copyOfRange(data, 1, size);
        size--;
    }

    /**
     * Adding a new element to the beginning of the array
     * @param obj
     * @param <E>
     */
    public <E> void pushFront(E obj) {
        ensureCapacity(1);
        size++;
        for (int i = size; i > 0; i--) {
            data[i] = data[i - 1];
        }
        data[0] = obj;
    }

    /**
     * Insert a new element into the array at the specified
     * index, checking for out-of-bounds array
     * @param index
     * @param obj
     * @param <E>
     */
    public <E> void insert(int index, E obj) {

        if (index > size || index < 0) {
            System.out.println("Out of bounds array");
        } else {
            data[index] = obj;
        }
    }

    /**
     * Delete one item at the specified index.
     * There must be an index validation check
     * @param index
     */
    public void removeAt(int index) {
        int num = size - index - 1;
        if (num > 0)
            System.arraycopy(data, index + 1, data, index,
                    num);
        data[--size] = null;
    }

    /**
     * Delete one element whose value
     * matches the value of the passed parameter
     * @param obj
     */
    public void remove(E obj) {
        label:
        for (int i = 0; i < size; i++) {
            if (obj.equals(data[i])) {
                int num = size - i - 1;
                if (num > 0)
                    System.arraycopy(data, i + 1, data, i,
                            num);
                data[--size] = null;
                break label;
            }
        }
    }

    /**
     * Delete all elements whose values
     * matches the value of the passed parameter
     * @param obj
     */
    public void removeAll(E obj) {
        for (int i = 0; i < size; i++) {
            if (obj.equals(data[i])) {
                int num = size - i - 1;
                if (num > 0)
                    System.arraycopy(data, i + 1, data, i,
                            num);
                data[--size] = null;
            }
        }
    }

    /**
     * Zeroing the array - to all elements of the array by
     * set indices from 0 to size-1 to null, field size
     * assign value 0
     */
    public void clear() {

        for (int i = 0; i < size; i++) {
            data[i] = null;
        }
        size = 0;
    }

    /**
     * Returns true if size = 0, and false in otherwise
     * @return
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Adjusts the value of capacity to size
     */
    public void trimToSize() {
        if (size < data.length) {
            data = Arrays.copyOf(data, size);
        }
    }

    /**
     * Linear search from left to first occurrence into an array
     * of the specified value. As a result of work, return
     * index of the item found, and if nothing is found, return -1
     * @param obj
     * @return
     */
    public int indexOf(E obj) {
        for (int i = 0; i < size; i++) {
            if (obj.equals(data[i]))
                return i;
        }
        return -1;
    }

    /**
     * Linear search from right to first occurrence
     * into an array of the specified value. As a result of work, return
     * index of the item found, and if nothing is found,
     * return -1
     * @param obj
     * @return
     */
    public int lastIndexOf(E obj) {
        for (int i = size - 1; i >= 0; i--) {
            if (obj.equals(data[i]))
                return i;
        }
        return -1;
    }
    /**
     * reordering elements in an array to the opposite
     */
    public void reverse() {
        for (int i = 0; i < size / 2; i++) {
            Object temp = data[i];
            data[i] = data[size - 1 - i];
            data[size - 1 - i] = temp;
        }
    }

    /**
     * Shuffle elements in an array
     */
    public void shuffle() {
        Random rnd = new Random();
        for (int i = 0; i < size; i++) {
            int index = rnd.nextInt(i + 1);
            Object a = data[index];
            data[index] = data[i];
            data[i] = a;
        }
    }

    /**
     * As a parameter, a link to another object of class MyArrayList.
     * Method compares arrays not only by the number of elements,
     * but also by their content
     * @param obj
     * @return
     */
    public boolean equals(MyArray obj) {
        if (size != obj.size) {
            return false;
        } else {
            for (int i = 0; i < size; i++) {
                if (!data[i].equals(obj.data[i])) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Return size
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     * Return a copy of the array element at the specified
     * index, checking for out-of-bounds array
     * @param index
     * @return
     */
    public E getElementAt(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return (E) data[index];
    }

    @Override
    public MyArray clone() throws CloneNotSupportedException {
        return (MyArray) super.clone();
    }

    /**
     * checks enough whether there is a memory reserve for storing the parameter specified
     * number of items. If the parameter value is less current capacity,
     * then nothing happens. If the value parameter is greater than the current capacity,
     * then the array recreates - it is allocated 2 times more memory.
     * Existing elements are wrapped into a new array.
     * @param numberOfElements
     */
    private void ensureCapacity(int numberOfElements) {
        if (numberOfElements - (capacity - size) > 0) {

            data = Arrays.copyOf(data, 2 * data.length);
            capacity = data.length;
        }
    }

    @Override
    public String toString() {
        String values = "";
        for (int i = 0; i < size; i++) {
            values += data[i] + " ";
        }
        return values;
    }
}

