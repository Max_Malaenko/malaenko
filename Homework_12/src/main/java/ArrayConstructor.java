import java.util.Random;

public class ArrayConstructor {


    public static int[] createArr(){
        Random rnd= new Random();
        int size=rnd.nextInt(9)+1;
        int[] arr=new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i]=rnd.nextInt(20)-10;
        }
        return arr;
    }

    public static  void printArr(int[] arr){

        System.out.print("Array:   ");
        for (int i = 0; i < arr.length; i++) {
            if(i==arr.length-1){
                System.out.print(arr[i]);
            }
            else
                System.out.print(arr[i] + "; ");
        }
    }

}
