//--//--//--//--//--//--// Task_03 //--//--//--//--//--//--//

public class Triangle {
    private final int firstSide;
    private final int secondSide;
    private final int thirdSide;

    public Triangle(int firstSide, int secondSide, int thirdSide){
         this.firstSide=firstSide;
         this.secondSide=secondSide;
         this.thirdSide=thirdSide;
         if((firstSide+secondSide<=thirdSide) | (firstSide+thirdSide<=secondSide) | (secondSide+thirdSide<=firstSide)){
             throw new MyUncheckedException(" Cannot build a triangle with these sides");
         }
    }

    public int getFirstSide() {
        return firstSide;
    }

    public int getSecondSide() {
        return secondSide;
    }

    public int getThirdSide() {
        return thirdSide;
    }
}
