import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystemAlreadyExistsException;
import java.nio.file.FileSystemNotFoundException;

public class Method {

//--//--//--//--//--// Task_01 //--//--//--//--//--//--//



    public void division() {
        long number = Input.inputNumber();
        if(number>Integer.MAX_VALUE){
              try {
                 throw new MyCheckedException("Too big number, try again");
              } catch (MyCheckedException e) {
                 System.out.println(e.getMessage());
                    division();
                 return;
            }
        }

        int[] arr = ArrayConstructor.createArr();
        ArrayConstructor.printArr(arr);
        System.out.println();
        System.out.println("Result:");

        for (int value : arr) {
            try {
                if(value==0) {
                    throw new ArithmeticException();
                }
                double result = (double) number / (double) value;
                System.out.printf("%.2f \n", result);
            } catch (ArithmeticException e) {

                System.out.print("Division by zero");
                System.out.println();
            }
        }

    }

    //--//--//--//--//--//--// Task_02 //--//--//--//--//--//--//
    public void checkNumber(int number) {
        if (number < 0) {
            try {
                throw new MyCheckedException("Number is less than zero");

            } catch (MyCheckedException e) {
                System.out.println(e.getMessage());

            }
        } else if (number > 100) {
            throw new MyUncheckedException("Number is more than 100");
        } else System.out.println("All is ok");
    }


    //--//--//--//--//--//--// Task_04 //--//--//--//--//--//--//
    public void binarySearch(int[] array, int key) throws MyCheckedException {

        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] >= array[i + 1]) {
                throw new MyCheckedException("Array is not sorted");
            }
        }

        int first = 0;
        int last = array.length - 1;
        int position = first + (last - first) / 2;

        while ((array[position] != key) && (first <= last)) {
            if (array[position] > key) {
                last = position - 1;
            } else {
                first = position + 1;
            }
            position = first + (last - first) / 2;
        }
        if (first <= last) {
            System.out.println("key index in the array= " + position);
        } else {
            System.out.println("key not found in array");
        }

    }


    //--//--//--//--//--//--// Task_05 //--//--//--//--//--//--//
    public void checkClassRunner() throws IOException {

        int i = 1;
        //noinspection InfiniteLoopStatement
        while (true) {

            new Runner().halt();
            System.out.println(" " + i);
            i++;
        }
    }


    //--//--//--//--//--//--// Task_06 //--//--//--//--//--//--//
    public void checkClassXmlReader(XmlReader[] obj) throws IOException {

        for (XmlReader o : obj) {
            try {
                o.read();
            } catch (FileSystemAlreadyExistsException e) {
                try {
                    throw new FileNotFoundException();
                } catch (FileNotFoundException ex) {
                    System.out.println(ex.getClass());
                }
            }catch (NullPointerException | Error | FileNotFoundException e){
                System.out.println(e.getClass());
            }
        }

    }

}


