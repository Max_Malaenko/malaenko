public class MyCheckedException extends Exception {

    public  MyCheckedException(String description){
        super(description);
    }
}
