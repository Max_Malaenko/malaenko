public class MyUncheckedException extends RuntimeException {

    public MyUncheckedException(String description){
        super(description);
    }
}
