import java.io.IOException;
import java.util.Random;

//--//--//--//--//--//--// Task_05 //--//--//--//--//--//--//

public class Runner {

    private static final Random rnd=new Random();

    public void halt() throws IOException {

        if(rnd.nextBoolean()){
            try {
                throw new RuntimeException();
            } catch (RuntimeException e) {
                System.out.print("halt");
            }
        }
        else {
            throw new IOException();
        }
    }
}
