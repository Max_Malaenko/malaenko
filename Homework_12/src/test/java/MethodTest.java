import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.rules.ExpectedException;

public class MethodTest {

    private Method method;

    @Before
    public void initObject() {
        Method method = new Method();
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @Test
    public void checkNumberTest() {
        exceptionRule.expect(MyUncheckedException.class);
        exceptionRule.expectMessage("Number is more than 100");
        Method method = new Method();
        method.checkNumber(110);
    }
    @Test
    public void constructorTriangleTest(){
        exceptionRule.expect(MyUncheckedException.class);
        exceptionRule.expectMessage(" Cannot build a triangle with these sides");
        Triangle triangle= new Triangle(1,2,3);
    }

}
