/*
Пользователь вводит с клавиатуры букву. Программа
должна определить, в какой раскладке введена буква – в ла-
тинской или кирилице. Вывести в консоль: «латиница», если
буква введена латиницей или «кирилица», если буква отно-
сится к кирилическому алфавиту. Если введена цифра, а не
буква, вывести «цифра». Если символ не относится ни к бук-
вам, ни к цифрам, вывести «невозможно определить».
 */

import java.util.Scanner;

public class Task07 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        String symbol =scan.nextLine();
        char a= symbol.charAt(0);
        if ( (a>=0x0410 && a<=0x044F) || a==0x0451 || a== 0x0401){
            System.out.println("Cyrillic");
        }
        else if(a>=0x0030 && a<=0x0039){
            System.out.println("Digit");
        }
        else if (a>=0x0041 && a<=0x007A){
            System.out.println("Latin");
        }
        else System.out.println("Impossible to determine");
    }
}
