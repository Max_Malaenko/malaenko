/*
Дана точка на плоскости заданная координатами x и y,
определить и вывести в консоль, в какой четверти находит-
ся точка, в прямоугольной (декартовой) системе координат.
 */

import java.util.Scanner;

public class Task04 {

    public static void main(String[] args) {
        Scanner scan =new Scanner(System.in);
        System.out.print("Enter coordinates: x y: ");
        String userInput= scan.nextLine();
        String[] stringArray = userInput.split(" ");

        int[] intArray = new int[stringArray.length];
        for (int i=0; i<stringArray.length; i++){
            intArray[i]=Integer.parseInt(stringArray[i]);
        }
        int x = intArray[0];
        int y = intArray[1];

        char first ='\u2160';
        char second='\u2161';
        char third ='\u2162';
        char fourth='\u2163';
        if (x>0 & y>0) {
            System.out.println(first );
        }
        else if (x<0 & y>0) {
            System.out.println(second);
        }
        else if (x<0 & y<0) {
            System.out.println(third);
        }
        else if (x>0 & y<0) {
            System.out.println(fourth);
        }
        else if(x==0) {
            System.out.println("Axis Y");
        }
        else if(y==0) {
            System.out.println("Axis X");
        }


    }

}
