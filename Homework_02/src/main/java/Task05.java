import java.util.Scanner;

/*
Организовать ввод с клавиатуры даты рождения чело-
века. Программа должна вывести знак зодиака и название
года по китайскому календарю.
Пример входных данных:
5 12 1974
Вывод:
Знак: Стрелец
Год: Тигра
 */
public class Task05 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter dd mm year");
        String dateOfBirth =scan.nextLine();
        String[] stringArray = dateOfBirth.split(" ");

        int[] intArray= new int[stringArray.length];
        for (int i=0; i<stringArray.length; i++) {
            intArray[i]=Integer.parseInt(stringArray[i]);
        }

        int day = intArray[0];
        int mounth = intArray[1];
        int year = intArray [2];

        if (mounth==1 ) {
            if (day>=20){
                System.out.println("Aquarius "+ '\u2652');
            }
            else {
                System.out.println("Capricorn "+ '\u2651');
            }
        }
        else if (mounth==2 ) {
            if (day>=19){
                System.out.println("Pisces "+ '\u2653');
            }
            else {
                System.out.println("Aquarius "+ '\u2652');
            }
        }
        else if (mounth==3 ) {
            if (day>=21){
                System.out.println("Aries "+ '\u2648');
            }
            else {
                System.out.println("Pisces "+ '\u2653');
            }
        }
        else if (mounth==4 ) {
            if (day>=20){
                System.out.println("Taurus "+ '\u2649');
            }
            else {
                System.out.println("Aries "+ '\u2648');
            }
        }
        else if (mounth==5 ) {
            if (day>=21){
                System.out.println("Gemini "+ '\u264A');
            }
            else {
                System.out.println("Taurus "+ '\u2649');
            }
        }
        else if (mounth==6 ) {
            if (day>=21){
                System.out.println("Cancer "+ '\u264B');
            }
            else {
                System.out.println("Gemini "+ '\u264A');
            }
        }
        else if (mounth==7 ) {
            if (day>=23){
                System.out.println("Leo "+ '\u264C');
            }
            else {
                System.out.println("Cancer "+ '\u264B');
            }
        }
        else if (mounth==8 ) {
            if (day>=23){
                System.out.println("Virgo "+ '\u264D');
            }
            else {
                System.out.println("Leo "+ '\u264C');
            }
        }
        else if (mounth==9 ) {
            if (day>=23){
                System.out.println("Libra "+ '\u264E');
            }
            else {
                System.out.println("Virgo "+ '\u264D');
            }
        }
        else if (mounth==10 ) {
            if (day>=23){
                System.out.println("Scorpio "+ '\u264F');
            }
            else {
                System.out.println("Libra "+ '\u264E');
            }
        }
        else if (mounth==11 ) {
            if (day>=22){
                System.out.println("Sagittarius "+ '\u2650');
            }
            else {
                System.out.println("Scorpio "+ '\u264F');
            }
        }
        else if (mounth==12 ) {
            if (day>=22){
                System.out.println("Capricorn "+ '\u2651');
            }
            else {
                System.out.println("Sagittarius "+ '\u2650');
            }
        }

        int ostatok = year - (year/12)*12;
                switch (ostatok) {
            case 0:
                System.out.println("Year: Monkey");
                break;
            case 1:
                System.out.println("Year: Rooster");
                break;
            case 2:
                System.out.println("Year: Dog");
                break;
            case 3:
                System.out.println("Year: Pig");
                break;
            case 4:
                System.out.println("Year: Rat ");
                break;
            case 5:
                System.out.println("Year: Ox");
                break;
            case 6:
                System.out.println("Year: Tiger");
                break;
            case 7:
                System.out.println("Year: Rabbit");
                break;
            case 8:
                System.out.println("Year: Dragon");
                break;
            case 9:
                System.out.println("Year: Snake");
                break;
            case 10:
                System.out.println("Year: Horse");
                break;
            case 11:
                System.out.println("Year: Ram");
                break;

        }


    }


}

