import java.util.Scanner;

/*
Написать программу, которая предлагает пользователю
выбрать животное из списка (1 – кошка, 2 – собака и т.д.), и в
ответ показывает, какие звуки издает выбранное животное.
В списке должно быть не менее 10 животных.
 */
public class Task03 {

    public static void main(String[] args) {

        System.out.println("Choose an animal:\n1-Cat\n2-Dog\n3-Cow\n4-Lion\n5-Bear\n6-Rooster\n7-Pig\n8-Owl\n9-Frog" +
                "\n10-Ship");

        Scanner animal = new Scanner(System.in);
        int number = animal.nextInt();

        if (number == 1) {
            System.out.println("The cat says: meaou ");
        } else if (number == 2) {
            System.out.println("The dog says: bov-vow ");
        } else if (number == 3) {
            System.out.println("The cow says: moo-moo ");
        } else if (number == 4) {
            System.out.println("The lion says: r-r-r ");
        } else if (number == 5) {
            System.out.println("The bear says: gr-gr-gr ");
        } else if (number == 6) {
            System.out.println("The rooster says: cock-a-doodle-doo ");
        } else if (number == 7) {
            System.out.println("The pig says: squeal-squeal ");
        } else if (number == 8) {
            System.out.println("The owl says: to-who ");
        } else if (number == 9) {
            System.out.println("The frog says: quack-quack ");
        } else if (number == 10) {
            System.out.println("The ship says: ba-a-ba-a ");
        } else {
            System.out.println("Wrong number");
        }
    }
}
