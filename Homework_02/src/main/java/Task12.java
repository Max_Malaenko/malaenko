import java.util.Scanner;

/*
Программа запрашивает шестизначное число. После вво-
да определяет, будет ли являться «счастливым» билет с та-
ким номером (сумма первых трех цифр совпадает с суммой
трех последних).
Пример входных данных:
423027
954832
Вывод:
Да
Нет
 */
public class Task12 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Insert the number");
        int number = scan.nextInt();
        int[] digit = new int[6];

        int i=0;
        while (number>0){
            digit[i]=number%10;
            number/=10;
            i++;
        }

        int sum1=digit[0]+digit[1]+digit[2];
        int sum2=digit[3]+digit[4]+digit[5];
        if (sum1==sum2) {
            System.out.println("YES");
        }
        else System.out.println("NO");

    }
}
