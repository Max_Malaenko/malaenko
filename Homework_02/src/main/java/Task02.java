/*
С клавиатуры вводится время (количество часов от 0 до
24) – программа выводит приветствие, соответствующее
введенному времени (например, ввели 15 часов – выводится
приветствие «Добрый день»).
 */

import java.util.Scanner;

public class Task02 {

    public static void main(String[] args) {

        System.out.println("Enter the Time");
        Scanner scanner = new Scanner(System.in);

        int hour= scanner.nextInt();
        if (hour>=5 && hour<=11){
            System.out.println("Good morning");
        }
        else if (hour>11 && hour<=17){
            System.out.println("Good day");
        }
        else if (hour>17 && hour <=23){
            System.out.println("Good evening");
        }
        else if (hour>23 && hour<=24 || hour>=0 && hour<5){
            System.out.println("Good night");
        }
    }
}
