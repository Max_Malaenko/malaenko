import java.util.Scanner;

/*
Найти корни квадратного уравнения и вывести их на
экран, если они есть. Если корней нет, то вывести сообще-
ние об этом. Конкретное квадратное уравнение определяет-
ся коэффициентами a, b, c, которые вводит пользователь с
клавиатуры.
 */
public class Task11 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a b c:");
        String roots = input.nextLine();
        String[] rootsArray =roots.split(" ");

        int[] rootsInt = new int[rootsArray.length];
        for (int i = 0; i<rootsArray.length; i++) {
            rootsInt[i]= Integer.parseInt(rootsArray[i]);
        }
        int a = rootsInt[0];
        int b = rootsInt[1];
        int c = rootsInt[2];

        if (a==0){
            System.out.println("This is not quadratic equation");
        }
        else {
            int discriminant =((int) Math.pow(b,2))- 4*a*c;
            if (discriminant<0){
                System.out.println("No valid roots");
            }
            else if(discriminant ==0) {
                System.out.println("x1 =x2 = " + (-b/(2*a)));
            }
            else {
                double x1= (-b-Math.sqrt(b*b-4*a*c))/(2*a);
                double x2= (-b+Math.sqrt(b*b-4*a*c))/(2*a);
                System.out.println("x1 = "+x1+"\n"+"x2 = "+x2);
            }
        }
    }
}
