import java.util.Scanner;

import static java.lang.Math.ceil;

/*
Определить номер подъезда девятиэтажного дома, по
указанному номеру квартиры N. Считать, что на каждом
этаже находится M квартир. Вывести в консоль номер подъ-
езда.
 */
public class Task10 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter flat number:");
        int flatNumber = scan.nextInt();
        System.out.println("Enter number of flat on one floor:");
        int flatONFloor = scan.nextInt();

       int flatInFrontDoor = 9* flatONFloor;

        int numberOfFrontDoor = (int) Math.ceil((double) flatNumber/ (double) flatInFrontDoor);
        System.out.println(numberOfFrontDoor);
    }
}
