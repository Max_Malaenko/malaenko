import java.util.Scanner;

/*
Даны два числа x и y. Программа должна вывести в кон-
соль YES, – если оба числа четные, либо оба числа нечетные;
иначе программа ничего не выводит.
 */
public class Task08 {

    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);
        System.out.println(" Enter first number");
        int firstNumber = userInput.nextInt();
        System.out.println("Enter second number");
        int secondNumber = userInput.nextInt();

        if ((firstNumber%2==0 && secondNumber%2==0) ||(firstNumber%2!=0 && secondNumber%2!=0)) {
            System.out.println("YES");
        }
    }
}
