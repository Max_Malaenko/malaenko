import java.util.Scanner;

/*
Даны координаты начала и координаты конца отрезка.
Если считать отрезок обозначением горки, то в одном слу-
чае он обозначает спуск, в другом – подъем. Определить и
вывести на экран – спуск это или подъем, ровная дорога или
вообще отвесная.
 */
public class Task09 {

    public static void main(String[] args) {

        Scanner scan =new Scanner(System.in);
        System.out.println("Enter coordinates of first drop: x1 y1");
        String first = scan.nextLine();
        String[] firstArray = first.split(" ");

        int[] firstInt = new int[firstArray.length];
        for (int i=0; i<firstArray.length; i++) {
            firstInt[i]=Integer.parseInt(firstArray[i]);
        }
        int x1= firstInt[0];
        int y1= firstInt[1];

        System.out.println("Enter coordinates of second drop: x2 y2");
        String second = scan.nextLine();
        String[] secondArray = second.split(" ");

        int[] secondInt = new int[secondArray.length];
        for (int i=0; i<secondArray.length; i++) {
            secondInt[i]=Integer.parseInt(secondArray[i]);
        }
        int x2= secondInt[0];
        int y2= secondInt[1];

        if (x1==x2){
            System.out.println("Plummet(отвес)");
        }
        else if( y1==y2){
            System.out.println("Smooth road");
        }
        else if((x1<x2 && y1<y2) || (x2<x1 && y2<y1)){
            System.out.println("Rise");
        }
        else {
            System.out.println("Descent");
        }

    }
}
