package TaskManager;

import java.io.Serializable;
import java.time.LocalDate;

public class Task implements Serializable {
    private static final long serialVersionUID = 8008517589489959759L;

    private String name;
    private String description;
    private int priority;
    private LocalDate dateStart;
    private LocalDate dateEnd;
    private String perfomer;
    private boolean isDone;

    public static final class TaskBuilder {
        private String name;
        private String description;
        private int priority;
        private LocalDate dateStart;
        private LocalDate dateEnd;
        private String perfomer;
        private boolean isDone;

        public TaskBuilder() {
        }

        public static TaskBuilder aTask() {
            return new TaskBuilder();
        }

        public TaskBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public TaskBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public TaskBuilder withPriority(int priority) {
            this.priority = priority;
            return this;
        }

        public TaskBuilder withDateStart(LocalDate dateStart) {
            this.dateStart = dateStart;
            return this;
        }

        public TaskBuilder withDateEnd(LocalDate dateEnd) {
            this.dateEnd = dateEnd;
            return this;
        }

        public TaskBuilder withPerfomer(String perfomer) {
            this.perfomer = perfomer;
            return this;
        }

        public TaskBuilder withIsDone(boolean isDone) {
            this.isDone = isDone;
            return this;
        }

        public Task build() {
            Task task = new Task();
            task.setName(name);
            task.setDescription(description);
            task.setPriority(priority);
            task.setDateStart(dateStart);
            task.setDateEnd(dateEnd);
            task.setPerfomer(perfomer);
            task.isDone = this.isDone;
            return task;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public LocalDate getDateStart() {
        return dateStart;
    }

    public void setDateStart(LocalDate dateStart) {
        this.dateStart = dateStart;
    }

    public LocalDate getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(LocalDate dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getPerfomer() {
        return perfomer;
    }

    public void setPerfomer(String perfomer) {
        this.perfomer = perfomer;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                ", dateStart=" + dateStart +
                ", dateEnd=" + dateEnd +
                ", perfomer='" + perfomer + '\'' +
                ", isDone=" + isDone +
                '}';
    }
}
