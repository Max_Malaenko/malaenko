package TaskManager;

import PhoneBook.InputReader;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskManager implements Serializable {

    private static final long serialVersionUID = 7545892547876612598L;

    private static final String INPUT_NAME = "Input name";
    private static final String INPUT_DESCRIPTION = "Input description";
    private static final String INPUT_PRIORITY = "Input priority";
    private static final String INPUT_YEAR = "Input year";
    private static final String INPUT_MONTH = "Input month";
    private static final String INPUT_DAY_OF_MONTH = "Input day of month";
    private static final String INPUT_DATE_OF_CRETION = "Input date of cretion";
    private static final String INPUT_DATE_OF_END = "Input date of end";
    private static final String INPUT_PERFOMER = "Input perfomer";

    private List<Task> taskList = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);


    public void createNewTask() {

        final Task task = new Task.TaskBuilder()
                .withName(getUserInput(INPUT_NAME, TaskManager::getStringInput))
                .withDescription(getUserInput(INPUT_DESCRIPTION, TaskManager::getStringInput))
                .withPriority(getUserInput(INPUT_PRIORITY, TaskManager::getIntInput))
                .withDateStart(getUserInput(INPUT_DATE_OF_CRETION, TaskManager::getLocalDateInput))
                .withDateEnd(getUserInput(INPUT_DATE_OF_END, TaskManager::getLocalDateInput))
                .withPerfomer(getUserInput(INPUT_PERFOMER, TaskManager::getStringInput))
                .build();

        taskList.add(task);
        saveTasksInFile(this);
    }

    private void saveTasksInFile(TaskManager taskManager) {

        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("TaskManager"))) {
            outputStream.writeObject(taskManager);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public TaskManager returnTasksFromFile() {
        TaskManager taskManager = null;

        try (ObjectInputStream objectInput = new ObjectInputStream(new FileInputStream("TaskManager"))) {
            taskManager = (TaskManager) objectInput.readObject();

            for (Task t : taskManager.getTaskList()) {
                System.out.println(t);
            }

        } catch (ClassCastException e) {
            e.printStackTrace();
        } catch (EOFException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return taskManager;
    }

    public void markTaskAsDone(String name) {
        for (Task t : taskList) {
            if (t.getName().equals(name)) {
                t.setDone(true);
            }
        }
        saveTasksInFile(this);
    }

    private static String getStringInput() {
        return scanner.next();
    }

    private static int getIntInput() {
        return scanner.nextInt();
    }

    private static LocalDate getLocalDateInput() {
        int year = getUserInput(INPUT_YEAR, TaskManager::getIntInput);
        int month = getUserInput(INPUT_MONTH, TaskManager::getIntInput);
        int day = getUserInput(INPUT_DAY_OF_MONTH, TaskManager::getIntInput);
        return LocalDate.of(year, month, day);
    }

    public static <T> T getUserInput(String message, InputReader<T> inputReader) {
        printMessage(message);
        return inputReader.read();
    }

    private static void printMessage(String message) {
        System.out.println(message);
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

}

