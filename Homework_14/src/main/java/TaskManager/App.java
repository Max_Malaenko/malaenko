package TaskManager;

import java.util.Scanner;

public class App {

    public static final String INPUT_NAME_OF_TASK = "Input name of task";
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        TaskManager taskManager = new TaskManager();
        taskManager.setTaskList(taskManager.returnTasksFromFile().getTaskList());
        System.out.println();

        System.out.println("1. Create new task\n" + "2. Mark task as done");
        int userChoice=scanner.nextInt();
        switch (userChoice) {
            case 1:
                taskManager.createNewTask();
                break;
            case 2:
                taskManager.markTaskAsDone(App.getUserInput(INPUT_NAME_OF_TASK, App::getStringInput));
                break;
        }
    }

    private static String getStringInput() {
        return scanner.next();
    }

    public static <T> T getUserInput(String message, InputReader<T> inputReader) {
        printMessage(message);
        return inputReader.read();
    }

    private static void printMessage(String message) {
        System.out.println(message);
    }
}
