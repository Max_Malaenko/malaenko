package TaskManager;

public interface InputReader<T> {
    T read();
}
