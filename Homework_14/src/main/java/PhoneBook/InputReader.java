package PhoneBook;

public interface InputReader <T> {
     T read();
}
