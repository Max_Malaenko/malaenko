package PhoneBook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ContactParser {


    public static final int FIELD_NAME_IN_ARRAY = 0;
    public static final int FIELD_LASTNAME_IN_ARRAY = 1;
    public static final int FIELD_NICKNAME_IN_ARRAY = 2;
    public static final int FIED_PHONE_NUMBERS_IN_ARRAY = 3;
    public static final int FIELD_EMAIL_IN_ARRAY = 4;
    public static final int FIELD_YEAR_OF_BIRTH_IN_ARRAY = 5;


    public static List<Contact> parsPhoneeBook() {

        List<Contact> contacts = new ArrayList<>();

        try (FileReader fileReader = new FileReader("PhoneBook.txt")) {
            BufferedReader reader = new BufferedReader(fileReader);
            String contactAsString = "";

            while ((contactAsString = reader.readLine()) != null) {
                String[] conntent = contactAsString.split(":");
                String name = conntent[FIELD_NAME_IN_ARRAY];
                String lastName = conntent[FIELD_LASTNAME_IN_ARRAY];
                String nickame = conntent[FIELD_NICKNAME_IN_ARRAY];
                String email = conntent[FIELD_EMAIL_IN_ARRAY];
                String year = conntent[FIELD_YEAR_OF_BIRTH_IN_ARRAY];
                int yearOfBirth = Integer.parseInt(year);

                String phoneNumbers = conntent[FIED_PHONE_NUMBERS_IN_ARRAY];
                String newPhoneNumbers = phoneNumbers.substring(1, phoneNumbers.length() - 1);
                String[] parts = newPhoneNumbers.split(", ");

                Map<String, TypePhoneNumber> numberList = new HashMap<>();
                for (int i = 0; i < parts.length; i++) {
                    String[] keyValye = parts[i].split("=");
                    numberList.put(keyValye[0], TypePhoneNumber.valueOf(keyValye[1]));
                }
                Contact contact =new Contact.Builder()
                        .name(name)
                        .lastName(lastName)
                        .nickname(nickame)
                        .numberList(numberList)
                        .email(email)
                        .yearOfBirth(yearOfBirth)
                        .build();
                contacts.add(contact);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contacts;
    }

}
