package PhoneBook;

import javafx.util.Builder;

import java.util.HashMap;
import java.util.Map;

public class Contact {

    private String name;
    private String lastName;
    private String nickname;
    private Map<String,TypePhoneNumber> numberList = new HashMap<String, TypePhoneNumber>();
    private String email;
    private int yearOfBirth;

    private Contact(Builder builder) {
        setName(builder.name);
        setLastName(builder.lastName);
        setNickname(builder.nickname);
        setNumberList(builder.numberList);
        setEmail(builder.email);
        setYearOfBirth(builder.yearOfBirth);
    }

    public static final class Builder {
        private String name;
        private String lastName;
        private String nickname;
        private Map<String, TypePhoneNumber> numberList;
        private String email;
        private int yearOfBirth;

        public Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder nickname(String nickname) {
            this.nickname = nickname;
            return this;
        }

        public Builder numberList(Map<String, TypePhoneNumber> numberList) {
            this.numberList = numberList;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder yearOfBirth(int yearOfBirth) {
            this.yearOfBirth = yearOfBirth;
            return this;
        }

        public Contact build() {
            return new Contact(this);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Map<String, TypePhoneNumber> getNumberList() {
        return numberList;
    }

    public void setNumberList(Map<String, TypePhoneNumber> numberList) {
        this.numberList = numberList;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    @Override
    public String toString() {
        return  name +":" +
                lastName+":" +
                nickname + ":" +
                numberList +":"+
                email + ":" +
                yearOfBirth ;
    }
}
