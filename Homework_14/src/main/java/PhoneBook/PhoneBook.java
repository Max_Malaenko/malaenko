package PhoneBook;

import java.io.*;
import java.util.*;

public class PhoneBook {

    private static List<Contact> contacts = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);

    private static final String INPUT_NAME = "Input name";
    private static final String INPUT_LAST_NAME = "Input lastname";
    private static final String INPUT_NICKNAME = "Input nickname";
    private static final String INPUT_EMAIL = "Input e-mail";
    private static final String INPUT_YEAR_OF_BIRTH = "Input year of birth";
    private static final String INPUT_PHONE_NUMBER = "Input phone number";


    public Contact createNewContact() {
        final Contact contact = new Contact.Builder()
                .name(getUserInput(INPUT_NAME, PhoneBook::getStringInput))
                .lastName(getUserInput(INPUT_LAST_NAME, PhoneBook::getStringInput))
                .nickname(getUserInput(INPUT_NICKNAME, PhoneBook::getStringInput))
                .yearOfBirth(getUserInput(INPUT_YEAR_OF_BIRTH, PhoneBook::getIntInput))
                .email(getUserInput(INPUT_EMAIL, PhoneBook::getStringInput))
                .build();

        Map<String, TypePhoneNumber> numberList = new HashMap<>();
        contact.setNumberList(numberList);
        contacts.add(contact);

        return contact;
    }

    public void addPhoneNumberToContact(Contact contact) {
        String phoneNumber = getUserInput(INPUT_PHONE_NUMBER, PhoneBook::getStringInput);

        System.out.println("Select type of phone number: \n" +
                "1. Home \n" +
                "2. Working \n" +
                "3. Mobile \n" +
                "4. Fax");
        int userChoice = scanner.nextInt();
        switch (userChoice) {
            case 1:
                contact.getNumberList().put(phoneNumber, TypePhoneNumber.HOME);
                break;
            case 2:
                contact.getNumberList().put(phoneNumber, TypePhoneNumber.WORKING);
                break;
            case 3:
                contact.getNumberList().put(phoneNumber, TypePhoneNumber.MOBILE);
                break;
            case 4:
                contact.getNumberList().put(phoneNumber, TypePhoneNumber.FAX);
                break;
        }

    }

    public void findContactByName(String name) {
        List<Contact> contactFromFile = new ArrayList<>();
        contactFromFile = ContactParser.parsPhoneeBook();
        contactFromFile.stream()
                .filter(contact -> contact.getName().equals(name))
                .forEach(System.out::println);
    }

    public void findContactByPhone(String phoneNumber) {
        List<Contact> contactFromFile = new ArrayList<>();
        contactFromFile = ContactParser.parsPhoneeBook();
        contactFromFile.stream()
                .filter(contact -> contact.getNumberList().containsKey(phoneNumber))
                .forEach(System.out::println);
    }

    private void savePhoneBookInFile(Contact contact) {

        String contactContent = contact.toString();

        try (FileWriter fr = new FileWriter("PhoneBook.txt", true)) {
            fr.write(contactContent + System.getProperty("line.separator"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showPhoneBook() {

        List<Contact> contactList = new ArrayList<>();
        contactList = ContactParser.parsPhoneeBook();
        for (Contact c : contactList) {
            System.out.println(c);
        }
    }

    private static String getStringInput() {
        return scanner.next();
    }

    private static int getIntInput() {
        return scanner.nextInt();
    }

    private static long getLongInput() {
        return scanner.nextLong();
    }

    public static <T> T getUserInput(String message, InputReader<T> inputReader) {
        printMessage(message);
        return inputReader.read();
    }

    private static void printMessage(String message) {
        System.out.println(message);
    }

    public static void main(String[] args) {
        PhoneBook phoneBook = new PhoneBook();
//        Contact kolya=phoneBook.createNewContact();
//        phoneBook.addPhoneNumberToContact(kolya);
//        phoneBook.addPhoneNumberToContact(kolya);
//        phoneBook.savePhoneBookInFile(kolya);
        //       phoneBook.showPhoneBook();
        phoneBook.findContactByPhone("375295489774");
    }
}
