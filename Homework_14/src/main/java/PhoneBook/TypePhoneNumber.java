package PhoneBook;

public enum TypePhoneNumber {
    HOME,
    WORKING,
    MOBILE,
    FAX
}
