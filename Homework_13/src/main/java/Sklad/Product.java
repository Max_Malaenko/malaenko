package Sklad;

import java.time.LocalDate;

public class Product {
    private String name;
    private String measure;
    private int price;
    private int quantity;
    private int sumCost;

    public Product(String name, String measure, int price, int quantity) {
        this.name = name;
        this.measure = measure;
        this.price = price;
        this.quantity = quantity;
        sumCost = price * quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSumCost() {
        return sumCost;
    }

    public void setSumCost() {
        sumCost = quantity * price;
    }

    @Override
    public String toString() {
        return name + "  " +
                measure +
                " price=" + price +
                ", quantity=" + quantity +
                ", sumCost=" + sumCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (price != product.price) return false;
        return name.equals(product.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + price;
        return result;
    }
}
