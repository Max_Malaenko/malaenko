package Sklad;

import java.time.LocalDate;

public class PowerOfAttorney {
    private int id;
    private LocalDate date;

    public PowerOfAttorney(int id, int year, int month, int dayOfMonth) {
        this.id = id;
        date = LocalDate.of(year, month, dayOfMonth);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }
}
