package Sklad;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class Invoice {
    private LocalDate date;
    private String fromWhom;
    private String toWhom;
    private PowerOfAttorney powerOfAttorney;
    private Set<Product> productList = new HashSet<>();
    private boolean isInternal; // true = если накладная для внутреннего перемещения

    public Invoice(int dayOfMonth, int month, int year, String fromWhom, String toWhom, PowerOfAttorney powerOfAttorney, boolean isInternal) {
        date = LocalDate.of(year, month, dayOfMonth);
        this.fromWhom = fromWhom;
        this.toWhom = toWhom;
        this.powerOfAttorney = powerOfAttorney;
        this.isInternal = isInternal;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getFromWhom() {
        return fromWhom;
    }

    public void setFromWhom(String fromWhom) {
        this.fromWhom = fromWhom;
    }

    public String getToWhom() {
        return toWhom;
    }

    public void setToWhom(String toWhom) {
        this.toWhom = toWhom;
    }

    public PowerOfAttorney getPowerOfAttorney() {
        return powerOfAttorney;
    }

    public void setPowerOfAttorney(PowerOfAttorney powerOfAttorney) {
        this.powerOfAttorney = powerOfAttorney;
    }

    public Set<Product> getProductList() {
        return productList;
    }

    public void setProductList(Set<Product> productList) {
        this.productList = productList;
    }

    public boolean isInternal() {
        return isInternal;
    }

    public void setInternal(boolean internal) {
        isInternal = internal;
    }
}
