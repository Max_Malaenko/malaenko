package Sklad;

import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;

public class Stock {
    private List<String> ownStocks = new ArrayList<>();
    private List<Product> productsInStock = new ArrayList<>();
    private Set<String> providers = new HashSet<>();
    private List<Invoice> invoiceList = new ArrayList<>();

    public void inputInvoice(Invoice invoice) {

        for (Product invoiceProduct : invoice.getProductList()) {
            if (productsInStock.contains(invoiceProduct)) {
                for (Product stockProduct : productsInStock) {
                    if (stockProduct.equals(invoiceProduct)) {
                        stockProduct.setQuantity(stockProduct.getQuantity() + invoiceProduct.getQuantity());
                        stockProduct.setSumCost();
                    }
                }
            } else {
                productsInStock.add(invoiceProduct);
            }
        }
        invoiceList.add(invoice);
        if (!invoice.isInternal()) {
            providers.add(invoice.getFromWhom());
        }
    }

    public void showProviders() {
        for (String pr : providers) {
            System.out.println(pr);

        }
    }

    public void findProductOnStock(Product pr) {
        productsInStock.stream()
                .filter(product -> product.getName().equals(pr.getName()))
                .forEach(System.out::println);
    }

    public void showAllProductsInStock(Stock stock) {
        for (Product p : stock.productsInStock) {
            System.out.println(p);
        }
    }

    public static void main(String[] args) {

        Stock stock = new Stock();
        Product milk = new Product("Milk", "litres", 2, 5);


        Product bread = new Product("Bread", "pieces", 1, 5);
        Set<Product> products = new HashSet<>();
        products.add(milk);
        products.add(bread);
        PowerOfAttorney powerOfAttorney = new PowerOfAttorney(1, 2019, 10, 25);
        Invoice invoice = new Invoice(5, 10, 2019, "ООО \"РОГА и копыта\"", "stock №3", powerOfAttorney, false);
        invoice.setProductList(products);
        stock.inputInvoice(invoice);
        stock.showAllProductsInStock(stock);
        System.out.println("--------------");

        Product salt = new Product("Salt", "gram", 1, 4);
        Product milk2 = new Product("Milk", "litres", 2, 10);
        Set<Product> products1 = new HashSet<>();
        products1.add(milk2);
        products1.add(salt);

        PowerOfAttorney powerOfAttorney1 = new PowerOfAttorney(2, 2019, 10, 26);
        Invoice invoice1 = new Invoice(6, 10, 2019, "ООО \"Шаражка\"", "stock №3", powerOfAttorney1, false);
        invoice1.setProductList(products1);
        stock.inputInvoice(invoice1);

        stock.showAllProductsInStock(stock);
        System.out.println();

        stock.showProviders();
        System.out.println("-----");
        stock.findProductOnStock(milk);
    }
}
