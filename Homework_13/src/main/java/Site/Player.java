package Site;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private String nickname;
    private List<Game> gameList = new ArrayList<>();

    public Player(String nickname) {
        this.nickname = nickname;
    }

    public void addGame(Game game) {
        gameList.add(game);
        game.rating.put(this, 0);
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public List<Game> getGameList() {
        return gameList;
    }

    public void setGameList(List<Game> gameList) {
        this.gameList = gameList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return nickname.equals(player.nickname);
    }

    @Override
    public int hashCode() {
        return nickname.hashCode();
    }

    @Override
    public String toString() {
        return nickname;
    }
}
