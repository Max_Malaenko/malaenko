package Site;

public class MyCheckException extends Exception {

    public MyCheckException(String description){
        super(description);
    }
}
