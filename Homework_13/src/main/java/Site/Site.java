package Site;

import java.util.*;

public class Site {

    public static final int NUMBER_OF_PLAYERS_IN_RATING = 10;
    private Map<Player, List<Game>> players = new HashMap<>();

    public void registrationPlayer(Player player) {
        if (players.containsKey(player)) {
            try {
                throw new MyCheckException("Such a player is already registered");
            } catch (MyCheckException e) {
                System.out.println(e.getMessage());
                System.out.println("Try again");
            }
        } else {
            players.put(player, player.getGameList());
        }
    }

    public void showGameList() {
        players.entrySet().stream()
                .flatMap(player -> player.getValue().stream())
                .distinct()
                .forEach(game -> System.out.print(game + " "));
    }

    public void addPointForVictory(Game game, Player player, int point) {
        if (player.getGameList().contains(game)) {
            int oldRating = game.rating.get(player);
            int newRating = oldRating + point;
            game.rating.replace(player, newRating);
        } else {
            try {
                throw new MyCheckException("Player is not registered in this game");
            } catch (MyCheckException e) {
                e.printStackTrace();
            }
        }
    }

    public void showPlayerRatingInGame(Game game, Player player) {
        if (player.getGameList().contains(game)) {
            game.rating.entrySet().stream()
                    .filter(playerIntegerEntry -> playerIntegerEntry.getKey().equals(player))
                    .forEach(System.out::println);
        } else {
            try {
                throw new MyCheckException("Player is not registered in this game");
            } catch (MyCheckException e) {
                e.printStackTrace();
            }
        }
    }

    public void showTenBestInGame(Game game) {
        System.out.println("Top-10 players in " + game.getName() + " :");
        game.rating.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .limit(NUMBER_OF_PLAYERS_IN_RATING)
                .forEach(System.out::println);
    }

    public void showBestAmountAllGame() {
        System.out.println("Top-10 players amount all games:");
        players.entrySet().stream()
                .flatMap(player -> player.getValue().stream())
                .distinct()
                .flatMap(game -> game.rating.entrySet().stream())
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .limit(NUMBER_OF_PLAYERS_IN_RATING)
                .forEach(System.out::println);
    }


    public static void main(String[] args) {
        Player player1 = new Player("Vasia");
        Player player2 = new Player("Petia");
        Player player3 = new Player("Kolya");
        Player player4 = new Player("max");
        Player player5 = new Player("oleg");
        Player player6 = new Player("andrey");
        Player player7 = new Player("alex");
        Player player8 = new Player("vova");
        Player player9 = new Player("kostya");
        Player player10 = new Player("sasha");
        Player player11 = new Player("igor");
        Player player12 = new Player("yura");
        Player player13 = new Player("dima");

        Game wow = new Game("WOW");
        Game cs = new Game("CS");
        Game lineAge2 = new Game("LineAge2");

        player1.addGame(wow);
        player1.addGame(cs);


        player2.addGame(lineAge2);
        player2.addGame(wow);

        player3.addGame(wow);

        player4.addGame(wow);
        player5.addGame(wow);
        player6.addGame(wow);
        player7.addGame(wow);
        player8.addGame(wow);
        player9.addGame(wow);

        player10.addGame(wow);
        player10.addGame(lineAge2);

        player11.addGame(wow);
        player12.addGame(wow);
        player13.addGame(wow);

        Site site = new Site();
        site.registrationPlayer(player1);
        site.registrationPlayer(player3);
        site.registrationPlayer(player10);

        site.addPointForVictory(wow, player1, 120);
        site.addPointForVictory(cs, player1, 190);

        site.addPointForVictory(wow, player2, 45);
        site.addPointForVictory(wow, player3, 12);
        site.addPointForVictory(wow, player4, 79);
        site.addPointForVictory(wow, player5, 100);
        site.addPointForVictory(wow, player6, 135);
        site.addPointForVictory(wow, player7, 19);
        site.addPointForVictory(wow, player8, 25);
        site.addPointForVictory(wow, player9, 55);

        site.addPointForVictory(wow, player10, 0);
        site.addPointForVictory(lineAge2, player10, 175);

        site.addPointForVictory(wow, player11, 130);
        site.addPointForVictory(wow, player12, 25);
        site.addPointForVictory(wow, player13, 11);


        site.showPlayerRatingInGame(wow, player9);

        site.showTenBestInGame(wow);
        site.showBestAmountAllGame();

    }


}
