package Site;

import java.util.HashMap;
import java.util.Map;

public class Game {

    private String name;
    public Map<Player, Integer> rating = new HashMap<>();

    public Game(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Player, Integer> getRating() {
        return rating;
    }

    public void setRating(Map<Player, Integer> rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return name;
    }
}
