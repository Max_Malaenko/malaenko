import org.junit.Assert;
import org.junit.Test;

public class MatrixTest {

    @Test
    public void CreateSingleMatrixTest() {
        int[][] expectedResultMatrix=  {{1,0},{0,1}};
        int[][] resultMatrix= new int[2][2];
        Matrix matrix = new Matrix();
        resultMatrix=matrix.createSingleMatrix(2);
        Assert.assertArrayEquals(expectedResultMatrix,resultMatrix);
    }


    @Test
    public void createZeroMatrixTest() {
        int[][] expectedResultMatrix = {{0,0,0},{0,0,0},{0,0,0}};
        int[][] resultMatrix= new int[3][3];
        Matrix matrix = new Matrix();
        resultMatrix=matrix.createZeroMatrix(3);
        Assert.assertArrayEquals(expectedResultMatrix,resultMatrix);
    }


    @Test
    public void additionMatrixTest() {
        Matrix matrix = new Matrix();
        int[][] firstMatrix={{1,2},{3,4}};
        int[][] secondMatrix={{1,2},{3,4}};
        int[][] expectedResultMatrix={{2,4},{6,8}};
        int[][] resultMatrix=matrix.additionMatrix(firstMatrix,secondMatrix);
        Assert.assertArrayEquals(expectedResultMatrix,resultMatrix);
    }

    @Test
    public void multiplicationSquareMatrixTest() {
        Matrix matrix = new Matrix();
        int[][] firstMatrix={{5,2},{3,1}};
        int[][] secondMatrix={{4,6},{5,2}};
        int[][] expectedResultMatrix={{30,34},{17,20}};
        int[][] resultMatrix=matrix.multiplicationSquareMatrix(firstMatrix,secondMatrix);
        Assert.assertArrayEquals(expectedResultMatrix,resultMatrix);
    }

    @Test
    public void multiplicationMatrixByScalarTest() {
        Matrix matrix=new Matrix();
        int[][] initialMatrix={{1,2},{3,4}};
        int scalar=3;
        int[][] expectedResultMatrix={{3,6},{9,12}};
        int[][] resultMatrix=matrix.multiplicationMatrixByScalar(scalar,initialMatrix);
        Assert.assertArrayEquals(expectedResultMatrix,resultMatrix);
    }
}
