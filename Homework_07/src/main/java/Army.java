// Используется в классе Battle
class Army {

    private int oneWarriorHealth;
    private int oneWarriorAttack;

    public void setOneWarriorHealth(int oneWarriorHealth) {
        this.oneWarriorHealth = oneWarriorHealth;
    }

    public int getOneWarriorHealth() {
        return oneWarriorHealth;
    }

    public void setOneWarriorAttack(int oneWarriorAttack) {
        this.oneWarriorAttack = oneWarriorAttack;
    }

    public int getOneWarriorAttack() {
        return oneWarriorAttack;
    }

    public int attackByArmy(int dragonHealth, int numberOfWarrior, int oneWarriorAttack){
        return dragonHealth-numberOfWarrior*oneWarriorAttack;
    }
}
