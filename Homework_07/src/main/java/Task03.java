import java.util.Arrays;
import java.util.Collections;

public class Task03 {

    public static void main(String[] args) {

        String[] arr={"aaa","cc","b"};
        String[] arr1={"333 333 333","4444 4444 4444 4444","1","22 22"};
        Task03 t= new Task03();
        t.printStringArray(arr1);
        String[] newArr=t.sortArr(arr);
        System.out.println();
        String[] newArr1=t.sortByWordCount(arr1);
        t.printStringArray(newArr1);


    }

    public void printStringArray(String[] a){
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i]+" ");
        }
    }

    public String[] sortArr(String[] a){
        Arrays.sort(a, Collections.reverseOrder());
        return a;
    }
    public String[] sortByWordCount(String[] a){

        int countWord[]= new int[a.length];

        for (int i = 0; i < a.length; i++) {
            String[] string=a[i].split(" ");
            countWord[i]=string.length;
        }
        int temp;
        String tempStr;
        for (int i = countWord.length-1; i > 0; i--) {
            for (int j = 0; j < i; j++) {

                if(countWord[j]>countWord[j+1]){
                temp=countWord[j];
                countWord[j]=countWord[j+1];
                countWord[j+1]=temp;

                tempStr=a[j];
                a[j]=a[j+1];
                a[j+1]=tempStr;
                }
            }
        }
        return a;
    }
}
