import java.util.Arrays;

//Task02
public class Array {

    public static void main(String[] args) {
        int[] arr={1,2,3,4,5,6,7};
        int[][] arr1={{1,2},{3,4,5,6},{9}};
        float[][] arr2={{3.5f,-2},{1.35f,4.5f}};
        String[] arr3={"мама","мыла","раму"};
        Array a= new Array();
        a.printArray(arr);
        System.out.println("\n");
        a.printArray(arr1);
        System.out.println();
        a.printArray(arr2);
        System.out.println();
        a.printArray(arr3);
    }

    public void printArray(int[] a){
        System.out.print("[");
        for (int i = 0; i < a.length; i++) {
            if (i==a.length-1){
                System.out.print(a[i]);
            }
            else System.out.print(a[i]+", ");
        }
        System.out.print("]");
    }

    public void printArray(int[][] a){
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j <a[i].length ; j++) {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }

    public void printArray(float[][] a){
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j <a[i].length ; j++) {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }

    public void printArray(String[] a){
        System.out.print("[");
        for (int i = 0; i < a.length; i++) {
            if (i == a.length - 1) {
                System.out.print("\"" + a[i] + "\"");
            } else {
                System.out.print("\"" + a[i] + "\", ");
            }
        }
        System.out.print("]");
    }

}
