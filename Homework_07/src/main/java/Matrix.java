public class Matrix {

    public int[][] createSingleMatrix(int n) {
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    matrix[i][j] = 1;
                } else {
                    matrix[i][j] = 0;
                }

            }
        }
        return matrix;
    }

    public int[][] createZeroMatrix(int n) {

        int[][] matrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = 0;
            }
        }
        return matrix;
    }

    public int[][] additionMatrix(int[][] a, int[][] b) {
        int[][] resultMatrix = new int[a.length][a.length];
        boolean isOneSize = true;
        if (a.length == b.length) {
            for (int i = 0; i < a.length; i++) {
                if (a[i].length != b[i].length) {
                    System.out.println("Different size from matrix");
                    isOneSize = false;
                    break;
                }
            }
        }
        else{
            isOneSize=false;
            System.out.println("Different size from matrix");
        }
        if (isOneSize) {
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a[i].length; j++) {
                    resultMatrix[i][j] = a[i][j] + b[i][j];
                }
            }
            return resultMatrix;
        }
        return resultMatrix;
    }

    public int[][] multiplicationSquareMatrix(int[][] a, int[][] b){
        int[][] resultMatrix= new int[a.length][a.length];
        boolean isOneSize = true;
        if (a.length == b.length) {
            for (int i = 0; i < a.length; i++) {
                if (a[i].length != b[i].length) {
                    System.out.println("Different size from matrix");
                    isOneSize = false;
                    break;
                }
            }
        }
        else{
            isOneSize=false;
            System.out.println("Different size from matrix");
        }
        if (isOneSize) {
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a.length; j++) {
                    for (int k = 0; k <a.length ; k++) {
                    resultMatrix[i][j]+=a[i][k]*b[k][j];
                    }
                }
            }
            return resultMatrix;
        }
        return resultMatrix;
    }

    public int[][] multiplicationMatrixByScalar(int scalar, int[][] a){
        int[][] resultMatrix= new int[a.length][a.length];

        for (int i = 0; i <a.length ; i++) {
            for (int j = 0; j < a.length; j++) {
                resultMatrix[i][j]=a[i][j]*scalar;
            }
        }
        return resultMatrix;
    }

    public void printMatrix(int[][] a){
        int N =a.length;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }

    }
}