// Используется в классе Battle
class Dragon {

   private int health;
   private int attack;

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHealth() {
        return health;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getAttack() {
        return attack;
    }

    public int AttackByDragon( int dragonAttack, int commonHelth){
        return commonHelth-dragonAttack;

    }
}
