import java.util.Scanner;
/*
Используются классы Dragon и Army
 */
public class Battle {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.print("Dragon health = ");
        int dragonHealth = scan.nextInt();
        System.out.println();
        System.out.print("Dragon attack = ");
        int dragonAttack = scan.nextInt();
        System.out.println();
        System.out.print("One warrior health =  ");
        int oneWarriorHealth = scan.nextInt();
        System.out.println();
        System.out.print("One warrior attack = ");
        int oneWarriorAttack = scan.nextInt();
        System.out.println();
        System.out.print("Enter number of warrior = ");
        int numberOfWarrior=scan.nextInt();
        System.out.println();
        scan.close();


        Battle b= new Battle();
        b.battle(dragonHealth,dragonAttack,oneWarriorHealth,oneWarriorAttack,numberOfWarrior);
    }

    public void battle(int dragonHealth,int dragonAttack, int oneWarriorHealth, int oneWarriorAttack, int N) {

        Dragon dragon = new Dragon();
        dragon.setHealth(dragonHealth);
        dragon.setAttack(dragonAttack);

        Army army = new Army();
        army.setOneWarriorHealth(oneWarriorHealth);
        army.setOneWarriorAttack(oneWarriorAttack);

        int numberOfWarrior = N;
        int commonHealth = numberOfWarrior * army.getOneWarriorHealth();

        while (dragonHealth > 0 | commonHealth > 0) {
            dragonHealth = army.attackByArmy(dragonHealth, numberOfWarrior, army.getOneWarriorAttack());
            if(dragonHealth<=0){
                System.out.println("Армия атакует - Дракон мёртв");
                break;
            }
            System.out.printf("Армия атакует(урон %d) - у дракона осталось %d жизней \n", numberOfWarrior * army.getOneWarriorAttack(), dragonHealth);

            commonHealth = dragon.AttackByDragon(dragon.getAttack(), commonHealth);
            numberOfWarrior = (int) Math.ceil((double)commonHealth /(double) army.getOneWarriorHealth());

            if(commonHealth<=0){
                System.out.println("Дракон атакует -Армия разбита");
                break;
            }
            if (commonHealth % army.getOneWarriorHealth() != 0) {
                int ost = commonHealth % army.getOneWarriorHealth();
                System.out.printf("Дракон атакует - осталось %d копейщиков, из них один ранен (%d жизней) \n", numberOfWarrior, ost);
            }
            else {
                System.out.printf("Дракон атакует - осталось %d копейщиков \n", numberOfWarrior);
            }

        }

    }
}
