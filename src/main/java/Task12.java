
/*
 * Шаблон для решения домашнеего задания. 
 */

import java.util.Scanner;

/**
 * Ученикам первого класса дают 1 пирожок, если вес первоклассника менее 30 кг
 * дополнительно дают 1 стакан молока и ещё 1 пирожок. В первых классах школы
 * учится n учеников. Стакан молока имеет ёмкость 200 мл, а упаковка молока –
 * 0,9 л. Написать программу которая определит количество пакетов молока и
 * пирожков, необходимых каждый день для условий: a) Если в школе 100% всех
 * учеников, у которых вес меньше 30 кг. b) Если в школе 60% учеников, имеют вес
 * меньше 30 кг. c) Если в школе 1% учеников, имеют вес меньше 30 кг.
 * 
 */
public class Task12
{
    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите количество учеников в первых классах");
        int n =s.nextInt();
        System.out.println("Введите процент детей, чей вес меньше 30 кг (100%, 60% или 1%)");
        int percent =s.nextInt();
        int numberOfGlases;
        int numberOfCakes = n;
        int numberOfPackage = 0;

        if(percent==100) {
            numberOfGlases = n;
            numberOfPackage=(int)Math.ceil(200*numberOfGlases/900);
            numberOfCakes=2*n;
        }
        else if(percent==60){
            numberOfGlases =(int)Math.ceil(0.6*n);
            numberOfPackage =(int)(Math.ceil(200*numberOfGlases/900));
            numberOfCakes +=(int)Math.ceil(0.6*n);
        }
        else if(percent==1){
            numberOfGlases =(int)Math.ceil(0.01*n);
            numberOfPackage =(int)Math.ceil(200*numberOfGlases/900);
            numberOfCakes +=(int)Math.ceil(0.01*n);
        }

        System.out.println("Необходимое кол-во пакетов молока = "+numberOfPackage+"\n"+
                            "Необходимое кол-во пирожков = "+ numberOfCakes);

    }
}
