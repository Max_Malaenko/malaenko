
/*
 * Шаблон для решения домашнеего задания. 
 */
/**
 * Написать программу расчета идеального веса к росту. В константах хранятся
 * рост (height) и вес (weight), вывести на консоль сообщение, сколько нужно кг
 * набрать или сбросить (идеальный вес = рост - 110). 
 * Вывод: положительное число если надо набрать вес, отрицательное если похудеть и ноль если вес идеальный
 */
public class Task10
{
    public static final int HEIGHT=198;
    public static final int WEIGHT=88;

    public static void main(String[] args)
    {
        int height = HEIGHT;
        int weight = WEIGHT;
        // две строки ниже нужны для автоматического теста
        height = (args.length == 2) ? Integer.valueOf(args[0]) : height;
        weight = (args.length == 2) ? Integer.valueOf(args[1]) : weight;

        // используй как входные данные переменные height и weight
        int raznica;
        if(height-110>weight){
           raznica =height-110-weight ;
           raznica= -raznica;
            System.out.println(raznica);
        }
        else if(height-110<weight){
            raznica=weight+110-height;
            System.out.println(raznica);
        }
        else {
            System.out.println(0);
        }
    }
}
