
/*
 * Шаблон для решения домашнеего задания. 
 */
/**
 * Шаблон для решения домашнеего задания
 * У Деда Мороза есть часы, которые в секундах показывают сколько осталось до
 * каждого Нового Года. Так как Дед Мороз уже человек в возрасте, то некоторые
 * математические операции он быстро выполнять не в состоянии. Помогите Деду
 * Морозу определить сколько полных дней, часов, минут и секунд осталось до
 * следующего Нового Года, если известно сколько осталось секунд, т. е.
 * Разложите время в секундах на полное количество дней, часов, минут и секунд.
 * Выведите результат на консоль.
 */
public class Task11
{
    public static void main(String[] args)
    {
        int SECONDS_TO_NEW_YEAR = 22896545;
        // данная строка нужна для автоматического теста (смотри Task11Test) 
        SECONDS_TO_NEW_YEAR = (args.length == 1) ? Integer.valueOf(args[0]) : SECONDS_TO_NEW_YEAR;
        // используй переменную SECONDS_TO_NEW_YEAR для решения

    int ostatok;

    int days =(int)SECONDS_TO_NEW_YEAR/86400;
    ostatok = SECONDS_TO_NEW_YEAR -days*86400;
    int hours = (int)(ostatok/3600);
    ostatok -= hours*3600;
    int minutes = (int)(ostatok/60);
    int seconds = ostatok - minutes*60;
        System.out.println("Дней "+"Часов "+"Минут "+"Секунд");
        System.out.println( days+" :   "+hours+" :   "+minutes+" :   " + seconds);

    }
}