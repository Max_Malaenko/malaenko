

/*
* Шаблон для решения домашнеего задания
*/

/**
 * Есть прямоугольник у которого известна ширина width и высота height, 
 * найти и вывести на консоль периметр и площадь заданного
 * прямоугольника. Высота и ширина прямоугольника должна задаваться константными
 * переменными в коде программы.
 * Вывод: сначала периметр, через пробел площадь 
 *
 */
public class Task06 {
    public static final int WIDTH=10;
    public static final int HEIGHT=5;

    public static void main(String[] args) {
        int width = WIDTH;
        int height = HEIGHT;

        // две строки кода ниже нужны для тестирования (смотри Task06Test) 
        width = (args.length == 2) ? Integer.valueOf(args[0]) : width;
        height = (args.length == 2) ? Integer.valueOf(args[1]) : height;
        // используй переменные width и height для нахождения результата

        Result.countPerimeter(width, height);
        Result.countSquare(width, height);
        Result.print();
    }

    static class Result {

        public static int perimeter;
        public static int square;

        public static int countPerimeter(int width, int height) {
            perimeter = 2 * (width + height);
            return perimeter;
        }

        public static int countSquare(int width, int height) {
            square = width * height;
            return square;
        }

        public static void print() {
            System.out.println(perimeter + " " + square);
        }
    }

}