
/*
 * Шаблон для решения домашнеего задания. 
 */

import java.sql.SQLOutput;

/**
 * В переменных х и y хранятся два натуральных числа. Создайте программу,
 * выводящую на консоль: 
 * • Результат целочисленного деления x на y • Остаток от деления x на y. 
 * • Квадратный корень x
 * 
 */
public class Task01
{
    public static void main(String[] args)
    {
        int x = 5;
        int y = 3;                                                                                                                      ;

        System.out.println("Результат целочисленного деления x на y = "+ x/y );
        System.out.println("Остаток от деления x на y = " +x%y);
        System.out.println("Квадратный корень x = "+ Math.sqrt(x));
    }
}