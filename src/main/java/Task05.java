
/*
* Шаблон для решения домашнеего задания
*/

import static java.lang.Math.PI;
import static java.lang.Math.pow;

/**
 * Подсчитать площадь и длину окружности для круга с радиусом R. Радиус задан
 * переменной с именем radius. Вывести результат на консоль.
 * Вывод: в одной строке через пробел, сначала окружность, потом площадь 
 */
public class Task05
{
    public static void main(String[] args) {
        int radius = 10;
        // данная строка нужна для тестирования (смотри Task05Test)
        radius = (args.length == 1) ? Integer.valueOf(args[0]) : radius;
        double square;
        double circleLenght;
        square =PI*Math.pow(radius,2);
        circleLenght=2*PI*radius;
        System.out.printf("%.3f", circleLenght);
        System.out.print(" ");
        System.out.printf("%.3f", square);

    }

}

