/*
* Шаблон для решения домашнеего задания
*/

import java.util.Scanner;

/**
 * Разработать программу, которая позволит при известном годовом проценте 
 * вычислить сумму вклада в банке через два года,
 * если задана исходная величина вклада. Вывести результат вычисления в консоль.
 *
 */
public class Task07
{
    public static void main(String[] args)
    {
        //int depositSumma = 1500;
        //int annualPercentage = 3;
        Scanner s = new Scanner(System.in);
        System.out.println("Введите сумму депозита");
        float depositSumma= s.nextFloat();
        System.out.println("Введите годовой процент");
        float annualPercentage =s.nextFloat();
        int year =2;
        float summaVklada =depositSumma;

        for(int i = 1; i <= year; i++){
            summaVklada = summaVklada  + summaVklada*annualPercentage/100;

        }
        System.out.println("Cумма вклада = "+ summaVklada);
    }

}
