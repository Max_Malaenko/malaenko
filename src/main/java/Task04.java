
/*
* Шаблон для решения домашнеего задания
*/
/**
 * Написать программу, которая выведет на консоль «Hello world» не используя в
 * исходном коде пробельных символов.
 * 
 *
 */
public class Task04 {
    public static void main(String[] args){
        char space='\u0020';
        System.out.println("Hello"+space+"world");
    }
}
