import java.util.Scanner;

/*
Обнулить крайний левый (старший разряд) единичный
бит числа N. Вывести результат на консоль в двоичном виде.
 */
public class Task08 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the number");
        int number = scan.nextInt();
        System.out.println(String.format("%32s", Integer.toBinaryString(number)).replace(' ', '0'));

        int metka=1 <<30;
        int index=0;

        while (number<metka) {
            metka>>=1;
            index++;
        }

        int newNumber= number &~(1<<(30-index));
        System.out.println("New number:"+"\n"+ String.format("%32s", Integer.toBinaryString(newNumber)).replace(' ', '0'));

    }
}
