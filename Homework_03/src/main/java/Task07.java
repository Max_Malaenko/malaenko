import java.util.Scanner;

/*
Определить значение i-го бита числа N. Вывести резуль-
тат на консоль в двоичном виде.
 */
public class Task07 {

    public static void main(String[] args) {

        Scanner scan =new Scanner(System.in);
        System.out.println("Enter the number");
        int number= scan.nextInt();
        System.out.println(String.format("%32s", Integer.toBinaryString(number)).replace(' ', '0'));

        System.out.println("Enter bit number");
        int bit= scan.nextInt();
        System.out.println((number & (1<<bit))>0 ? 1 : 0); //  Младший бит имеет номер 0
    }
}
