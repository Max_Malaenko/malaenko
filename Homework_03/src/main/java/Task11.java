import java.util.Scanner;

/*
Посчитать и вывести на консоль количество единичных
битов в числе N. Вывести на консоль исходное число в дво-
ичном виде и результат
 */
public class Task11 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the number");
        int number =scan.nextInt();
        System.out.println(Integer.toBinaryString(number));
        int sum=0;

        while(number!=0){
            sum+=(number&1);
            number>>>=1;
        }
        System.out.println(sum);
    }
}
