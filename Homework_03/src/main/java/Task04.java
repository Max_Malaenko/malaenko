import java.util.Scanner;

/*
Инвертировать i-й бит числа N. Вывести результат на
консоль в двоичном виде.
 */
public class Task04 {
    public static void main(String[] args) {

        Scanner scan =new Scanner(System.in);
        System.out.println("Enter the number");
        int number= scan.nextInt();

        System.out.println(String.format("%32s", Integer.toBinaryString(number)).replace(' ', '0'));

        System.out.println("Enter bit number");
        int bit= scan.nextInt();

        int number1 = number ^ (1 << bit); //  Младший бит имеет номер 0
        System.out.println("New number:"+"\n"+ String.format("%32s", Integer.toBinaryString(number1)).replace(' ', '0'));
    }
}
