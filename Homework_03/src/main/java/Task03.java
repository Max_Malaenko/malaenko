import java.util.Scanner;

/*
Установить i-й бит числа N равным 1. Вывести результат
на консоль в двоичном виде.
 */
public class Task03 {

    public static void main(String[] args) {

        Scanner scan =new Scanner(System.in);
        System.out.println("Enter the number");
        int number= scan.nextInt();

        System.out.println("Enter bit number");
        int bit= scan.nextInt();

        System.out.println(String.format("%32s", Integer.toBinaryString(number)).replace(' ', '0'));

        int number1 = number | (1 << bit); // Младший бит имеет номер 0
        System.out.println("New number:"+"\n"+ String.format("%32s", Integer.toBinaryString(number1)).replace(' ', '0'));
    }
}
