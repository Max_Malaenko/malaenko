import java.util.Scanner;

/*
Проверить, есть ли в двоичной записи числа n хотя бы
один 0. Используя только побитовые и условные операторы.
Вывести на консоль исходное число в двоичном виде и ре-
зультат.
 */
public class Task12 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the number");
        int number =scan.nextInt();
        System.out.println(Integer.toBinaryString(number));

        int sum=0;
        while (number!=0){
         if((number & 1)==0){
             sum+=1;
         }
         number>>>=1;
        }
        if (sum>0){
            System.out.println("YES");
        }
        else{
            System.out.println("NO");
        }
    }

}
