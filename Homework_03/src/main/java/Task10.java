import java.util.Scanner;

/*
Найти и вывести на консоль минимальное из двух чисел
M и N, используя только побитовые операции.
 */
public class Task10 {

    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the first number");
        int firstNumber= scan.nextInt();
        System.out.println("Enter the second number");
        int secondNumber= scan.nextInt();

        int min=firstNumber&((firstNumber-secondNumber)>>31) | secondNumber& (~(firstNumber-secondNumber)>>31);

        System.out.println("Minimum = "+min);
    }
}
