import java.util.Scanner;

/*
Установить i-й бит числа N равным 0. Вывести результат
на консоль в двоичном виде.
 */
public class Task05 {

    public static void main(String[] args) {

        Scanner scan =new Scanner(System.in);
        System.out.println("Enter the number");
        int number= scan.nextInt();
        System.out.println(Integer.toBinaryString(number));

        System.out.println("Enter bit number");
        int bit= scan.nextInt();

        int number1 = number &~(1 << bit);
        System.out.println("New number:"+"\n"+ Integer.toBinaryString(number1));
    }
}
