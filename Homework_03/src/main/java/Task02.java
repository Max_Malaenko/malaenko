/*
Вывести на консоль 2 в степени n. Для вычисления ис-
пользовать только побитовые операции.
 */
import java.util.Scanner;

public class Task02 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a power of two");
        int power =scan.nextInt();
        int number =2 << (power-1);
        System.out.println(String.format("%32s", Integer.toBinaryString(number)).replace(' ', '0'));

    }
}
