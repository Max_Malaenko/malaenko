import java.util.Scanner;

/*
Определить имеют ли числа M и N разные знаки. Исполь-
зуя только побитовые и условные операторы.
 */
public class Task09 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the first number");
        int firstNumber= scan.nextInt();
        System.out.println("Enter the second number");
        int secondNumber= scan.nextInt();

        int znak= firstNumber^secondNumber;

        if (znak>=0){
            System.out.println("Same");
        }
        else {
            System.out.println("Different");
        }

    }
}
