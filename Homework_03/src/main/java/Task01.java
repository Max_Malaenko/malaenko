import java.util.Scanner;

/*
Обнулить бит в нулевом разряде числа N. Остальные
биты не должны изменить свое значение. Вывести на кон-
соль в двоичном виде.
 */
public class Task01 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the number");
        int number1= scan.nextInt();
        System.out.println(Integer.toBinaryString(number1));

        int number2= number1 &~(1);
        System.out.println("New number: ");
        System.out.println(Integer.toBinaryString(number2));
    }
}
