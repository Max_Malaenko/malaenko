public class HourlyWorker extends Employee {

    private double rateOneHour;
    private int hours;
    private final static  double RATE=0.2; //курс обмена в тугрики
    private double salaryInTugrik;



    public double getRateOneHour() {
        return rateOneHour;
    }

    public void setRateOneHour(double rateOneHour) {
        this.rateOneHour = rateOneHour;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public HourlyWorker(String name,String lastName, boolean isHaveKids,boolean isOfshor){
        super(name,lastName,"Hourly payment", isHaveKids, isOfshor);
    }

    @Override
    public double countSalary(){
        salary=rateOneHour*hours;
        return salary;
    }

    @Override
    public String toString() {
        return  fio+" | "+formOfPayment+" | "+tax+" %"+" | "+ salary+" usd"+" | "+salaryWithoutTax/2+" usd"+"/"+salaryInTugrik+" tugrik";
    }

    public void convertSalary(){
        double half= salaryWithoutTax/2;
         salaryInTugrik = half/RATE;
    }
}
