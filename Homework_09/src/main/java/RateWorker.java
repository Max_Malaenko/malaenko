public class RateWorker extends Employee {

    private double rateOneDay;
    private int days;



    public double getRateOneDay() {
        return rateOneDay;
    }

    public void setRateOneDay(double rateOneDay) {
        this.rateOneDay = rateOneDay;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public RateWorker(String name, String lastName,boolean isHaveKids, boolean isOfshor){

        super(name, lastName,"Rate payment", isHaveKids, isOfshor);
    }

    @Override
    public double countSalary(){
        salary=rateOneDay*days;
        return salary;
    }

}
