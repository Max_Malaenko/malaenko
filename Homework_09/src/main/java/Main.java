import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {


        Employee first= new RateWorker("Ivan","Ivanov",true, false);
        ((RateWorker)first).setRateOneDay(50);
        ((RateWorker)first).setDays(5);

        Employee second= new HourlyWorker("Petr","Petrov",false,false);
        ((HourlyWorker)second).setRateOneHour(5);
        ((HourlyWorker)second).setHours(210);

        Employee third= new PieceWorker("Vasia","Sidorov", true,true);
        ((PieceWorker)third).setRateAllWork(350);
        ((PieceWorker)third).setPercent(75);


        ArrayList<Employee> staff = new ArrayList<Employee>();
        staff.add(0,first);
        staff.add(1,second);
        staff.add(2,third);

        for (Employee e: staff) {
            if (e.isHaveKids) e.raiseTax();
        }

        for (Employee e: staff) {
            if (e.isOfshor) e.deleteTax();
        }

        first.countSalary();
        second.countSalary();
        third.countSalary();
        first.countSalaryWithoutTax();
        second.countSalaryWithoutTax();
        third.countSalaryWithoutTax();

        for (Employee e: staff) {
            if (e instanceof HourlyWorker) {
                ((HourlyWorker) e).convertSalary();
            }
        }
        for (Employee e: staff) {
            if ((!e.isOfshor) && (e instanceof HourlyWorker) && ((HourlyWorker) e).getHours()>=200) {
                e.getPrize();
            }
        }

        System.out.println(first);
        System.out.println(second);
        System.out.println(third);


        String[] columns={"FIO","Form of payment","Tax %","Sum, usd","Without tax"};
        String[][] data={{first.getFio().toString(),first.getFormOfPayment(),Double.toString(first.getTax()),Double.toString(first.getSalary()),Double.toString(first.getSalaryWithoutTax())},
                        {second.getFio().toString(),second.getFormOfPayment(),Double.toString(second.getTax()),Double.toString(second.getSalary()),Double.toString(second.getSalaryWithoutTax())},
                        {third.getFio().toString(),third.getFormOfPayment(),Double.toString(third.getTax()),Double.toString(third.getSalary()),Double.toString(third.getSalaryWithoutTax())}};

        JTable table = new JTable(data,columns);
        JScrollPane scrollPane = new JScrollPane(table);
        JFrame frame= new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(scrollPane);
        frame.setPreferredSize(new Dimension(450, 200));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}
