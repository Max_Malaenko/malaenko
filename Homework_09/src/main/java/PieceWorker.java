public class PieceWorker extends Employee {

    private double rateAllWork;
    private int percent;


    public double getRateAllWork() {
        return rateAllWork;
    }

    public void setRateAllWork(double rateAllWork) {
        this.rateAllWork = rateAllWork;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public PieceWorker(String name, String lastName, boolean isHaveKids, boolean isOfshor){
        super(name,lastName,"Piece-work payment", isHaveKids, isOfshor);
    }

    @Override
    public double countSalary(){
        salary=rateAllWork*percent/100;
        return salary;
    }

}
