public class FIO {
    private String name;
    private String LastName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
    public FIO(String name, String LastName){
        this.name=name;
        this.LastName=LastName;
    }

    @Override
    public String toString() {
        return  LastName + " " + name ;
    }
}
