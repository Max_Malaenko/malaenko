public abstract class Employee {

    protected String formOfPayment;
    protected double salary;
    protected double salaryWithoutTax;
    protected FIO fio;
    protected int tax;
    protected boolean isHaveKids;
    protected boolean isOfshor;
    protected double hoursWorked;


    public Employee(String name, String lastName, String formOfPayment, boolean isHaveKids, boolean isOfshor) {
        this.formOfPayment = formOfPayment;
        this.isHaveKids = isHaveKids;
        this.isOfshor = isOfshor;

        if ((this instanceof RateWorker) | (this instanceof HourlyWorker)) {
            tax = 20;
        } else if (this instanceof PieceWorker) {
            tax = 15;
        }
        FIO f = new FIO(name, lastName);
        this.fio = f;
    }

    public abstract double countSalary();

    public double countSalaryWithoutTax() {
        salaryWithoutTax = (salary * (100 - tax)) / 100;
        return salaryWithoutTax;
    }

    public void raiseTax() {
        tax += 5;
    }

    public void deleteTax() {
        tax = 0;
    }

    public void getPrize(){
        salary=salary*1.1;
    }

    @Override
    public String toString() {
        return fio + " | " + formOfPayment + " | " + tax + " %" + " | " + salary + " usd" + " | " + salaryWithoutTax + " usd";
    }

    public String getFormOfPayment() {
        return formOfPayment;
    }

    public void setFormOfPayment(String formOfPayment) {
        this.formOfPayment = formOfPayment;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public FIO getFio() {
        return fio;
    }

    public void setFio(FIO fio) {
        this.fio = fio;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public double getSalaryWithoutTax() {
        return salaryWithoutTax;
    }

    public void setSalaryWithoutTax(double salaryWithoutTax) {
        this.salaryWithoutTax = salaryWithoutTax;
    }

    public boolean isHaveKids() {
        return isHaveKids;
    }

    public void setHaveKids(boolean haveKids) {
        isHaveKids = haveKids;
    }

    public boolean isOfshor() {
        return isOfshor;
    }

    public void setOfshor(boolean ofshor) {
        isOfshor = ofshor;
    }

    public double getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }
}