import java.util.Scanner;

/*
В первый день спортсмен пробежал x километров, а затем
он каждый день увеличивал пробег на 10% от предыдущего
значения. По числу, указанному с клавиатуры y, определите
номер дня, на который пробег спортсмена составит не менее
y километров.
 */
public class Task12 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the distance for the first day:");
        double firstDayDistance = scan.nextDouble();
        System.out.println("Enter the necessary distance:");
        double distance= scan.nextDouble();
        double range=firstDayDistance;
        int day=1;

        do{
           range=range*1.1;
           day++;
        }
        while(range<=distance);

        System.out.println(day);
    }
}
