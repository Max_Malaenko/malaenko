import java.util.Arrays;

/*
Вывести на консоль все восьмизначные числа, цифры в
которых не повторяются. Эти числа должны делиться на
12345, без остатка. Показать общее количество найденных
чисел.
 */
public class Task06 {

    public static void main(String[] args) {
        int sum = 0;

        for (int number = 10000000; number < 100000000; number++) {
            char[] digitArr = Integer.toString(number).toCharArray();
            Arrays.sort(digitArr);
            boolean isNorm =true;

            for (int i = 0; i < digitArr.length-1; i++) {

                if (digitArr[i] == digitArr[i + 1]) {
                   isNorm=false;
                    break;
                }
            }
                if((isNorm) && (number%12345==0))  {
                   System.out.print(number+" ");
                   sum++;
                }
            }
        System.out.println();
        System.out.println("Amount of numbers = "+ sum);
        }
}

