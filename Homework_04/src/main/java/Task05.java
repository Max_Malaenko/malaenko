import java.util.Scanner;

/*
Напишите программу, которая будет проверять, является
ли число, введенное с клавиатуры палиндромом (одинаково
читающееся в обоих направлениях). Например, 123454321
или 221122 – палиндром. Программа должна вывести YES,
если число является палиндромом, и NO – в противополож-
ном случае.
 */
public class Task05 {

    public static void main(String[] args) {

        Scanner scan= new Scanner(System.in);
        System.out.println("Enter the number");
        char[] digitArr= scan.nextLine().toCharArray();
        boolean isPolindrom =true;
        int lenght= digitArr.length -1;

        for (int i=0; i<lenght-i; i++){
            if (digitArr[i]!=digitArr[lenght-i]){
                    isPolindrom = false;
            }

         }
        if (isPolindrom){
            System.out.println("YES");
        }
        else {
            System.out.println("NO");
        }
        scan.close();
    }
}
