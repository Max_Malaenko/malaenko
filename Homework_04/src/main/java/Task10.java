/*
*Задание 10
2520 – наименьшее число, которое можно разделить на
каждое из чисел от 1 до 10, без остатка. Написать програм-
му, которая рассчитывает наименьшее положительное чис-
ло, которое делится на все числа от 1 до 20.
 */
public class Task10 {

    public static void main(String[] args) {

        for (int number = 2520; ; number++) {
            boolean isMultiple = true;

            for (int devisor = 1; devisor <= 20; devisor++) {
                if (number % devisor != 0) {
                    isMultiple = false;
                    break;
                }
            }
            if (isMultiple) {
                System.out.println(number);
                break;
            }
        }
    }
}