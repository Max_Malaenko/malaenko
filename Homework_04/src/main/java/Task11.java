/*
Напишите программу, которая выводит на экран числа
от 1 до 1 000. При этом вместо чисел, кратных трём, про-
грамма должна выводить слово fizz, а вместо чисел, кратных
пяти – слово buzz. Если число кратно пятнадцати, то про-
грамма должна выводить вместо числа слово hiss.
 */
public class Task11 {

    public static void main(String[] args) {

        for(int number=1; number<=1000; number++) {
            if (number%3==0 && number%5==0){
                System.out.print("hiss ");
            }
            else if(number%5==0) {
                System.out.print("buzz ");
            }
            else if(number%3==0){
                System.out.print("fizz ");
            }
            else System.out.print(number+" ");
        }
    }
}
