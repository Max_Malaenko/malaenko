/*
число N является простым, если оно больше 1 и при этом делится без остатка
только на 1 и на N (на самого себя).
Написать программу, которая выводит на экран все про-
стые числа в диапазоне от 2 до 1 000 000.
 */
public class Task02 {

    public static void main(String[] args) {



       for(int i=2; i<=1000000; i++){

           boolean isSimple =true;
             for(int j=2; j<i; j++){
                if (i%j==0){
                isSimple=false;
                break;
                }
             }

            if (isSimple) {
                System.out.print(i + " ");

            }
        }
    }
}
