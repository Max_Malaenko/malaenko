/*
Совершенное число – натуральное число, равное сумме всех
своих собственных делителей (то есть всех положительных
делителей, отличных от самого числа).
Показать на экране все совершенные числа в диапазоне от
0 до 1 000 000.
 */
public class Task04 {

    public static void main(String[] args) {

        for(int number=0; number<=1000000; number++) {
            int sum=0;
            for(int divisor=1; divisor<number; divisor++) {

                if (number%divisor==0){
                    sum+=divisor;
                }
            }
            if (sum==number){
                System.out.print(number+" ");
            }
        }
    }
}
