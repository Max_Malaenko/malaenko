/*
Если перечислить все натуральные числа меньше 10, крат-
ные 3 или 5, мы получаем 3, 5, 6 и 9. Сумма этих кратных – 23.
Найдите сумму всех чисел, кратных 3 или 5, начиная с 0
и до 1 000.
 */
public class Task09 {

    public static void main(String[] args) {

        int sum=0;

        for(int i=0; i<=1000; i++) {
            if(i%3==0 | i%5==0){
                sum++;
                System.out.print(i+" ");
            }
        }
        System.out.println();
        System.out.println(sum);
    }
}
