/*
Электронные часы показывают время в формате от 00:00
до 23:59. Написать программу, которая выведет на консоль
сколько раз за сутки случается так, что слева от двоеточия
показывается симметричная комбинация для той, что спра-
ва от двоеточия (например, 02:20, 11:11 или 15:51). Вывести
на экран все симметричные комбинации. Вывести общее
число комбинаций.
 */
public class Task08 {

    public static void main(String[] args) {

        int hours =0;
        int minutes =0;
        int sumOfSymmetries=0;

        while (hours<24) {
            int h1=hours/10;
            int h2=hours%10;
            int m1=minutes/10;
            int m2=minutes%10;

            if(h1==m2 && h2==m1){
                sumOfSymmetries++;
                System.out.printf("%d%d : %d%d   ",h1,h2,m1,m2);
            }
            minutes++;
            if (minutes==60){
                minutes=0;
                hours++;
            }
        }
        System.out.println();
        System.out.println("Number of symmetries =" +sumOfSymmetries);
    }
}
