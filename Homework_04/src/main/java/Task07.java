/*
Показать битовое представление значения переменной
типа int, используя только один цикл, управляющую пере-
менную, вывод на консоль и битовые операции.
Не использовать строки и любые другие готовые функ-
ции (методы).
 */

import java.util.Scanner;

public class Task07 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number");
        int number = scan.nextInt();

        System.out.println(dectoBin(number));

    }

    private static int dectoBin(int n) {
        int ostatok=n%2;
        int result=n/2;
        if(n!=1){
            System.out.print(dectoBin(result));
        }
        else{
            System.out.print(1);
            return ostatok;
        }
        return ostatok;

    }
}



