/*
Самовлюблённое число или число Армстронга – натураль-
ное число, которое равно сумме своих цифр, возведенных
в степень, равную количеству его цифр.
Показать на экране все числа Армстронга в диапазоне от
10 до 1 000 000.
 */
public class Task03 {
    public static void main(String[] args) {

        for(int number=10; number<=1000000; number++) {
                        String numberAsString = Integer.toString(number);
            int numberOfDigit= numberAsString.length();
            int[] digitArr= new int[numberOfDigit];
            int sum=0;
            int numberForCycle=number;

             for (int j=0; j<numberOfDigit; j++){
                digitArr[j]= numberForCycle%10;
                numberForCycle=numberForCycle/10;
                sum= sum+(int)Math.pow(digitArr[j],numberOfDigit);
            }
             if (sum==number){
                 System.out.println(number+" ");
             }

        }
    }
}
